<?php

/**
 * @link       http://webnus.biz
 * @since      1.0.0
 *
 * @package    Depper Comment
 */

class Depc_Model_Public_Comment_Loadmore extends Depc_Model_Public_Comment {

	public $validator;
	public $ajax_action  = 'dpr_loadmore';
	private static $nounce  = 'dpr_loadmore';

	/**
	 * Constructor
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		// init validator
		$this->validator = new Depc_Request_Validator;
		$this->settings = $this->get_user_settings();
		$this->settings['default_sorting'] = $this->validator->get_comment_order( $this->settings['default_sorting'] );


	}

	/**
	 * 	
	 */
	public function render() {

		// security check
		check_ajax_referer( self::$nounce, 'security' );

		$comments = $this->load_halfcomments( $_POST['id'], $_POST['loaded_cm'] , $_POST['type'] );

		// remined comments
		$half_cm = (int) $_POST['half_cm'] - (int) $this->settings['loadmore_count'] ;

		// loaded comment
		$loaded_cm = $this->settings['loadmore_count'] + $_POST['loaded_cm'];

		$loop = Depc_Controller_Public_Comment_Loop::get_comment_loop();
		$response = $loop->load( $comments , $_POST['id'] );

		wp_send_json( array( 'data' => $response , 'half_cm' => $half_cm, 'loaded_cm' => $loaded_cm  ) );

	}

	public function load_halfcomments( $id , $offset , $order ) {

		$args = array(
			'post_id'	=> $id,
			'number'	=> $this->settings['loadmore_count'],
			'status'	=> 'approve',
			'parent'	=> 0,
			'offset'	=> $offset,
			'order'		=> $order
			);

		$depc_query = $this->get_query();
		$comments = $depc_query->query( $args );
		return $comments;

	}

	/**
	 * @return get parent comment count
	 */
	public static function parent_comments( $id ) {

		global $wpdb;

		$query = "SELECT COUNT(comment_post_id) AS count FROM $wpdb->comments WHERE `comment_approved` = 1 AND `comment_post_ID` = $id AND `comment_parent` = 0";
		$parents = $wpdb->get_row($query);
		return $parents->count;
		
		// $args = array(
		// 	'post_id'	=> $id,
		// 	'status'	=> 'approve',
		// 	'parent'	=> 0,
		// 	'count'		=> true
		// 	);

		// $depc_query = new WP_Comment_Query;
		// $comments = $depc_query->query( $args );
		// return $comments;

	}	

	/**
	 * @return js scripts
	 */
	public static function scripts() {

		$id = ( new self )->post_id();
		$nounce = wp_create_nonce( self::$nounce );

		// Generating javascript code tpl
		$javascript = '
			jQuery(document).ready(function() {
				jQuery(".dpr-discu-container_'.$id.' .dpr-discu-main-loop-wrap .dpr-loadmore-btn").depcLoadMore({
					id: "'.$id.'",
					nounce : "'. $nounce .'",
					action: "dpr_loadmore"
				});
			});';

			return $javascript;

	}

}