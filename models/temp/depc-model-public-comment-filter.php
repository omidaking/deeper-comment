<?php

/**
 * @link       http://webnus.biz
 * @since      1.0.0
 *
 * @package    Depper Comment
 */

class Depc_Model_Public_Comment_Filter extends Depc_Model_Public_Comment {

	public $validator;
	public $ajax_action  = 'dpr_filter';
	private static $nounce  = 'dpr_filter';
	private $settings;

	/**
	 * Constructor
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		// init validator
		$this->validator = new Depc_Request_Validator;

		// get settings
		$this->settings = $this->take_settings();

	}

	public function render() {

		check_ajax_referer( self::$nounce, 'security' );

		if ( $_POST['type'] == 'trending' ) 
			$this->trending();
		elseif ( $_POST['type'] == 'popular') 
			$this->popular();
		elseif ( $_POST['type'] == 'oldest')
			$this->oldest( $_POST['id'] );
		elseif ( $_POST['type'] == 'newest')
			$this->newest( $_POST['id'] );

	}

	public function trending() {
		
		// Arguments for the query
		$args = array(
			'number' => '2',
			'status' => 'approve',
			'meta_query' => array(
				array(
					'key' => 'dpr_social',
					'orderby' => 'meta_value_num',
					'order' => DESC

				),
				array(
					'key'     => 'dpr_vote',
					'orderby' => 'like_count',
					'order' => DESC
				),				
				array(
					'key'     => 'dpr_vote',
					'orderby' => 'dislike_count',
					'order' => DESC
				),

			)

		);

		$depc_query = $this->get_query();
		$comments = $depc_query->query( $args );

		$a = Depc_Controller_Public_Comment_Loop::get_comment_loop();
		$s = $a->load( $comments );
		// update_option( $option, $newvalue );
		wp_send_json( $s );


	}	

	public function popular() {
		
		// Arguments for the query
		$args = array(
			'number' => '2',
			'status' => 'approve',
			'meta_query' => array(
				array(
					'key' => 'dpr_social',
					'orderby' => 'meta_value_num',
					'order' => DESC

				),
				array(
					'key'     => 'dpr_vote',
					'orderby' => 'like_count',
					'order' => DESC
				),				
				array(
					'key'     => 'dpr_vote',
					'orderby' => 'dislike_count',
					'order' => DESC
				),

			)

		);

		$depc_query = $this->get_query();
		$comments = $depc_query->query( $args );

		$a = Depc_Controller_Public_Comment_Loop::get_comment_loop();
		$s = $a->load( $comments );
		// update_option( $option, $newvalue );
		wp_send_json( $s );


	}

	public function oldest( $id ) {
		
		// Arguments for the query
		$args = array(
			'post_id' => $id,
			'number' => '5',
			'status' => 'approve',
			'order'	=> 'ASC'
		);

		$depc_query = $this->get_query();
		$comments = $depc_query->query( $args );

		$a = Depc_Controller_Public_Comment_Loop::get_comment_loop();
		$s = $a->load( $comments );

		// update_option( $option, $newvalue );
		wp_send_json( $s );


	}
	public function newest( $id ) {
		
		// Arguments for the query
		$args = array(
			'post_id' => $id,
			'number' => '5',
			'status' => 'approve',
			'order'	=> 'DESC'
		);

		$depc_query = $this->get_query();
		$comments = $depc_query->query( $args );

		$a = Depc_Controller_Public_Comment_Loop::get_comment_loop();
		$s = $a->load( $comments );
		// update_option( $option, $newvalue );

		wp_send_json( $s );

	}

	/**
	 * @return js scripts
	 */
	public static function scripts() {

		$id = ( new self )->post_id();
		$nounce = wp_create_nonce( self::$nounce );
		// Generating javascript code tpl
		$javascript = '
			jQuery(document).ready(function() {
				jQuery(".dpr-discu-container_'.$id.' .dpr-switch-tab-wrap .dpr-switch-tab .dpr-switch-tab-trending .dpr-switch-tab-trending-a").depcFilter({
					id: "'.$id.'",
					nounce : "'. $nounce .'",
					action : "dpr_filter",
					type: "trending"
				});					
				jQuery(".dpr-discu-container_'.$id.' .dpr-switch-tab-wrap .dpr-switch-tab .dpr-switch-tab-trending .dpr-switch-tab-trending-a").depcFilter({
					id: "'.$id.'",
					nounce : "'. $nounce .'",
					action : "dpr_filter",
					type: "trending"
				});					
				jQuery(".dpr-discu-container_'.$id.' .dpr-switch-tab-wrap .dpr-switch-tab .dpr-switch-tab-oldest .dpr-switch-tab-oldest-a").depcFilter({
					id: "'.$id.'",
					nounce : "'. $nounce .'",
					action : "dpr_filter",
					type: "oldest"
				});					
				jQuery(".dpr-discu-container_'.$id.' .dpr-switch-tab-wrap .dpr-switch-tab .dpr-switch-tab-newest .dpr-switch-tab-newest-a").depcFilter({
					id: "'.$id.'",
					nounce : "'. $nounce .'",
					action : "dpr_filter",
					type: "newest"
				});				
			});';

			return $javascript;

	}

}