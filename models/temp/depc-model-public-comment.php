<?php

/**
 * @link       http://webnus.biz
 * @since      1.0.0
 *
 * @package    Depper Comment
 */

class Depc_Model_Public_Comment extends Depc_Model_Public {


	public $ajax_action  = 'dpr_add_comment';
	private static $nounce  = 'dpr_add_comment';
	private $settings;
	protected $deeper_query;


	/**
	 * Constructor
	 *
	 * @since    1.0.0
	 */
	protected function __construct() {

		$this->register_hook_callbacks();

	}

	public function register_hook_callbacks() {

		// get settings
		$this->settings = $this->take_settings();
		$this->user_setting = $this->get_user_settings();

	}

	public function get_query() {
		$this->deeper_query = new WP_Comment_Query;
		return $this->deeper_query;
	}
	
	/**
	 * Show admin notices if plugin activation had any error
	 *
	 * @since    1.0.0
	 */
	public static function show_comment_template() {

		$array = array(
			'path' => '',
			'params' =>  self::scripts()
		);

		if ( is_user_logged_in() == true ) {
			$array['path'] = Depc_Core::get_depc_path() . '/views/tpl/logged-in-comment.php';
			return $array;

		}else {
			$array['path'] = Depc_Core::get_depc_path() . '/views/tpl/not-log-comment.php';
			return $array;
		}
	}

	public function render() {
		// -1 comment is empty
		check_ajax_referer( self::$nounce, 'security' );


		// if recaptcha is on and user is guest
		if ( $this->settings['recaptcha'] == 'google' && $this->settings['recaptchacm'] == 'on' && !is_user_logged_in() ) {
			$g_recaptcha_response = isset( $_POST['recaptcha'] ) ? $_POST['recaptcha'] : NULL;
			if ( $g_recaptcha_response == NULL ) wp_send_json( array( 'status' => -1 , 'data' => __( 'Please set recaptcha.', 'depc') ) ); 
			if( !$this->get_recaptcha_response( $g_recaptcha_response ) ) wp_send_json( array( 'status' => -1 , 'data' => __('Captcha is invalid! Please try again.', 'depc') ) );
		}
		
		// check for whos try to submit comments
		$current_user = parent::get_current_user();
		$author = $this->get_comment_author();
		$usermail = $current_user->user_email;
		$user_url = $current_user->user_url;
		$user_id = $current_user->ID;

		if ( !is_user_logged_in() ) {
			$author = sanitize_text_field( $_POST['gemail'] ) ;
			$usermail = sanitize_email( $_POST['gname'] );
			$user_url = '';
			$user_id = 0 ;
		}

		$validator = new Depc_Request_Validator;
		$comment_status = 'hold';

		// guest checking
		if ( $validator->is_guest_allowed() == 0 ) {
			_e('Please Loggin for post Comment.', 'depc' );
			wp_die();
		}

		// validation checking 
		if ( $validator->terms_empty( $_POST['comment_data'] ) == -1 ) wp_send_json( array( 'status' => '-1' , 'data' => esc_attr__( 'Please write your comment.','depc' ) ) );

		// approve	
		if ( $validator->may_i_approve() == 1 ) {
			$comment_status = 'approve';
		}

		// get commenter user info
		$current_user = parent::get_current_user();
		// get time 
		$time = current_time('mysql');

		// get comment id parent if exist
		$comment_id = false;
		if ( isset( $_POST['comment_id'] ) ) {
			$comment_id = Depc_Request_Validator::is_numeric( $_POST['comment_id'] , false );
		}

		$comment_id = ( $comment_id != false ) ? $comment_id : 0 ;

		$commentdata = array(
			'comment_post_ID' => sanitize_key( $_POST['post_id'] ),
			'comment_author' => $author, 
			'comment_author_email' =>  $usermail, 
			'comment_author_url' => $user_url, 
			'comment_content' =>  wp_rel_nofollow( wp_kses_data( $_POST['comment_data'] ) ), 
			'comment_type' => '',
			'comment_parent' => $comment_id,
			'comment_date' => $time,
			'user_id' => $user_id,
		);
		$comment_id = wp_new_comment( $commentdata );
		wp_set_comment_status( $comment_id, $comment_status );

		$publish_or_not = $validator::is_numeric( $comment_id , false );

		if (  $publish_or_not != false && $comment_id != -1 ) {
			$renderd_data = $this->maybe_publish_comment( $comment_id , $commentdata , $comment_status );
			wp_send_json( array( 'status' => 'publish' , 'data' => $renderd_data , 'newcomment_id' => $comment_id ) );
		} else {
			wp_send_json( array( 'status' => 'hold' , 'data' => $comment_id ) );
		}

	}


	/**
	 * User script for comment inserting
	 *
	 * @since    1.0.0
	 */
	private static function scripts() {

		$id = ( new self )->post_id();
		$settings = ( new self )->settings;
		$nounce = wp_create_nonce( self::$nounce );
		$logged = parent::get_current_user();

		// Generating javascript code tpl
		$javascript = '
		jQuery(document).ready(function() {
			jQuery(".dpr-discu-container_'.$id.' .dpr-join-form textarea").depcSubmitComment({
				id: "'.$id.'",
				nounce : "'. $nounce .'",
				reply : false,
				selector : ".dpr-join-form textarea",
				logged   : "'.$logged->ID.'",
				captcha : "' .$settings['recaptchacm']. '"

			});			
			jQuery(".dpr-discu-container_'.$id.' .dpr-discu-wrap .dpr-discu-box-footer .dpr-discu-reply-btn").depcSubmitComment({
				id: "'.$id.'",
				nounce : "'. $nounce .'",
				reply : true,
				selector : ".dpr-discu-wrap .dpr-discu-box-footer .dpr-discu-reply-btn",
				logged   : "'.$logged->ID.'",
				captcha : "' .$settings['recaptchacm']. '"
			});			

			jQuery(".dpr-discu-container_'.$id.' .dpr-discu-wrap .dpr-discu-replies-wrap .dpr-tinymce-button .dpr_add_reply_comment").depcSubmitComment({
				id: "'.$id.'",
				nounce : "'. $nounce .'",
				reply : 1,
				selector : ".dpr-discu-wrap .dpr-discu-replies-wrap .dpr-tinymce-button .dpr_add_reply_comment"
			});
		});
		';
		
		return $javascript;

	}

	public function avatar( $mail , $type ) {

		$core = static::get_core();

		$is_genrate = $core::get_option( 'dc_generate_avatar','Avatar' );

		if( $this->has_gravatar( $mail ) ) {

			$out = get_avatar( $mail, 50 );
			return $out;

		} elseif ( $is_genrate == 'on' ) {

			return $this->get_dp_avatar( $type );

		} else {

			$out = get_avatar( $mail, 50 );
			return $out;

		}

	}


	private function get_dp_avatar( $type ) {

		$text = $this->get_user_text( $type );
		$text = $this->make_avatar( $text );
		return $text;

	}

	private function has_gravatar( $email_address ) {

		$url = 'http://www.gravatar.com/avatar/' . md5( strtolower( trim ( $email_address ) ) ) . '?d=404';

		$headers = @get_headers( $url );

		return preg_match( '|200|', $headers[0] ) ? true : false;

	}

	private function make_avatar( $text ){

		$text = strtoupper( substr( $text , 0,1 ) );
		$color = $this->random_color();
		return '<div style="color:'.$color.';" data-dprletters="'.$text.'"></div>';

		$args = array(
			'text'      => substr( $text , 0,1 ),
			'width'     => 50,
			'height'    => 50,
			'bcolor'    => $this->random_color(),
			'txtxcolor' => 'ffffff'
		);

		$letterAvatar = new Depc_Avatar( $args );
		$letterAvatar = $letterAvatar->getImg();
		return $letterAvatar ;

	}

	public function get_user_text( $type ) {

		if ( $type != 'current' ) return $type;

		$current_user = parent::get_current_user();
		if ( $current_user->user_firstname && $current_user->user_lastname ) {

			return $current_user->user_firstname . ' ' . $current_user->user_lastname;

		} elseif ( $current_user->user_firstname ) {

			return $current_user->user_firstname;

		} elseif ( $current_user->user_lastname ) {

			return $current_user->user_lastname;

		} elseif ( $current_user->display_name ) {

			return $current_user->display_name;

		} else {

			return $current_user->user_login;

		}

	}

	private function get_comment_author() {

		if ( is_user_logged_in() == 'true' ) 
			return $this->get_user_text( 'current' );
		else
			return 'hii';

	}		

	private function maybe_publish_comment( $comment_id, $commentdata , &$comment_status ) {

		$comment_status = ( $comment_status == 'approve' ) ? 'dpr-discu-wrap' : 'dpr-discu-wrap dpr-discu-pending';

		$out = '
		<div id="comments-'.$comment_id.'" class="'.$comment_status.'">

			<div class="dpr-discu-user">
				<div class="dpr-discu-user-img">
				'.$this->avatar( $commentdata['comment_author_email'] , $commentdata['comment_author'] ).'
				</div>
			</div>

			<div class="dpr-discu-box">
				<div class="dpr-discu-box-header"> 
					<span class="dpr-discu-user-name">
						<span> '.$commentdata['comment_author'].'</span>
					</span> 
				<span class="dpr-discu-date">
					<span>'.$commentdata['comment_date'].'</span>
				</span>

				<div class="clearfix"></div>

				<div class="dpr-discu-text">
					<p>'.$commentdata['comment_content'].'</p>
				</div>

			</div>

		</div>';

		return $out; 

	}	

	public function get_user_profile_link() {

		$current_user = parent::get_current_user();
		$link = get_edit_user_link( $current_user->ID );
		return $link;
	}


	protected function random_color_part() {
		return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
	}

	protected function random_color() {
		
		$current_user = parent::get_current_user();
		$user_defined = get_the_author_meta( 'dpr_comment' , $current_user->ID  );

		if ( $this->user_setting['generate_avatar'] == 'on' &&  isset( $user_defined ) && $user_defined != '' ) {
			// $user_defined = ltrim($user_defined, '#');
			return $user_defined;
		}
		return '#' . $this->random_color_part() . $this->random_color_part() . $this->random_color_part();
	}


}