<?php

/**
 * @link       http://webnus.biz
 * @since      1.0.0
 *
 * @package    Depper Comment
 */

class Depc_Model_Public_Comment_Vote extends Depc_Model_Public_Comment {

	public $validator;
	public $ajax_vote_action  = 'dpr_vote';
	private static $nounce  = 'dpr_vote';
	private $settings;

	/**
	 * Constructor
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		// init validator
		$this->validator = new Depc_Request_Validator;

		// set action vote cockie for guest
		// Depc_Actions_Filters::add_action( 'init', $this, 'set_coockie' );
		Depc_Actions_Filters::add_action( 'wp_print_scripts', $this, 'scripts' );
		// get expirtions settings
		$this->settings['user_date']  	= Depc_Core::get_option( 'dc_vote_user_date', 'Vote_Options' );
		$this->settings['guest_date']  	= Depc_Core::get_option( 'dc_vote_guest_date', 'Vote_Options' );

	}

	/**
	 * 	
	 */
	public function render() {

		// security check
		check_ajax_referer( self::$nounce, 'security' );

		// get validator object
		$logged_in = $this->validator;

		// process guest users
		if ( $logged_in->is_registered() == false ) {
			wp_send_json( 'not_logged' );
		}

		// process logged in users
		$response = $this->update_user_vote( trim( $_POST['comment_id'] ) );
		wp_send_json( $response );

	}

	/**
	 * Set coockie for vote
	 */
	public function set_coockie() {
		
		$logged_in = $this->validator;
		if ( $logged_in->is_registered() == false ) {
			if( !isset( $_COOKIE['dpr_vote'] ) ) {

				setcookie( 'dpr_vote', '', 30 * DAYS_IN_SECONDS, COOKIEPATH, COOKIE_DOMAIN );
			}
		}

	}	

	/**
	 * @return js scripts
	 */
	public static function scripts() {

		$id = ( new self )->post_id();
		$nounce = wp_create_nonce( self::$nounce );
		// Generating javascript code tpl
		$javascript = '
			jQuery(document).ready(function() {
				jQuery(".dpr-discu-container_'.$id.' .dpr-discu-box .dpr-discu-box-footer-metadata-like .dpr-cont-discu-like .dpr-discu-like").depcVote({
					id: "'.$id.'",
					nounce : "'. $nounce .'",
					like: "like"
				});				
				jQuery(".dpr-discu-container_'.$id.' .dpr-discu-box .dpr-discu-box-footer-metadata-like .dpr-cont-discu-dislike .dpr-discu-dislike").depcVote({
					id: "'.$id.'",
					nounce : "'. $nounce .'",
					like: "dislike"
				});
			});';

			return $javascript;

	}

	/**
	 * @return Ip Address
	 */
	public function get_client_ip() {

		$ipaddress = '';
		if (isset($_SERVER['HTTP_CLIENT_IP']))
			$ipaddress = $_SERVER['HTTP_CLIENT_IP'];
		else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
			$ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
		else if(isset($_SERVER['HTTP_X_FORWARDED']))
			$ipaddress = $_SERVER['HTTP_X_FORWARDED'];
		else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
			$ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
		else if(isset($_SERVER['HTTP_FORWARDED']))
			$ipaddress = $_SERVER['HTTP_FORWARDED'];
		else if(isset($_SERVER['REMOTE_ADDR']))
			$ipaddress = $_SERVER['REMOTE_ADDR'];
		else
			$ipaddress = 'UNKNOWN';
		return $ipaddress;

	}	

	/**
	 * @return Ip Address
	 */
	public function update_user_vote( $id ) {

		// user who voted
		$current_user = static::get_current_user();
		
		// get vote meta data
		$voted = get_comment_meta( $id, 'dpr_vote', true );
		$voted['liked_user'] = isset( $voted['liked_user'] ) ? $voted['liked_user'] : $voted['liked_user'] = array() ;

		// process logged in users
		if ( $_POST['like'] == 'like' ) {

			$voted['like_count'] = isset( $voted['like_count'] ) ? ++$voted['like_count'] : 1 ;

			if ( in_array( $current_user->ID , $voted['liked_user'] ) != 1 ) {
				// update user id
				$voted['liked_user'][] = $current_user->ID;
				$voted['liked_user'][$current_user->ID]['time'] = current_time( 'mysql' );
				// update count
				$voted['like_count'] = ++$voted['like_count'];
				update_comment_meta( $id , 'dpr_vote', $voted );
				return 1;
			} else {
				// calculate for date diffrence
				$from = strtotime( $voted['liked_user'][$current_user->ID]['time'] );
				$today =  strtotime( current_time( 'mysql' ) );
				$difference = $today - $from;
				$get_expirtion_date = strtotime( $this->settings['user_date'] . 'day' , time() );

				if ( $difference > $get_expirtion_date ) {
					$voted['liked_user'][$current_user->ID]['time'] = current_time( 'mysql' );
					// update count
					$voted['like_count'] = ++$voted['like_count'];
					update_comment_meta( $id , 'dpr_vote', $voted );
					return 1;
				}else{
					return 0;
				}
			}

		} elseif (  $_POST['like'] == 'dislike' ) {
			
			$voted['dislike_count'] = isset( $voted['dislike_count'] ) ? ++$voted['dislike_count'] : 1 ;

			if ( in_array( $current_user->ID , $voted['liked_user'] ) != 1 ) {
				// update user id
				$voted['liked_user'][] = $current_user->ID ;
				$voted['liked_user'][$current_user->ID]['time'] = current_time( 'mysql' );
				// update count
				$voted['dislike_count'] = ++$voted['dislike_count'];
				update_comment_meta( $id , 'dpr_vote', $voted );
				return 1;

			} else {
				// calculate for date diffrence
				$from = strtotime( $voted['liked_user'][$current_user->ID]['time'] );
				$today =  strtotime( current_time( 'mysql' ) );
				$difference = $today - $from;
				$get_expirtion_date = strtotime( $this->settings['user_date'] . ' day', time() );

				if ( $difference > $get_expirtion_date ) {
					$voted['liked_user'][$current_user->ID]['time'] = current_time( 'mysql' );
					// update count
					$voted['dislike_count'] = ++$voted['dislike_count'];
					update_comment_meta( $id , 'dpr_vote', $voted );
					return 1;
				}else{
					return 0;
				}
			}

		}
	}

}