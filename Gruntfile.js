'use strict';
module.exports = function(grunt) {

    grunt.initConfig({

        uglify: {

            options: {
            },
            target: {
                files: {
                  'views/js/deeper.min.js' : ['views/js/package/jquery-confirm.js','views/js/package/clipboard.js', 'views/js/package/deeper.js']
                }
            }
        },
        // ccs min task
        cssmin: {
              options: {
                  banner: '/* Webnus.biz */',
                  inline: ['all'] 
                },
              target: {
                files: {
                  'views/css/deeper.min.css' : ['views/css/package/deeper.css','views/css/package/jquery-confirm.css']
                }
              },
        },
        watch: {
            css: {
                files: ['views/css/package/*.css'],
                tasks: ['cssmin']
            },
            js: {
                files: ['views/js/package/*.js'],
                tasks: ['uglify']
            }
        },
        pot: {
            options:{
                text_domain: 'depc', //Your text domain. Produces my-text-domain.pot
                dest: 'languages/', //directory to place the pot file
                keywords: [ 'xgettext','__' ], //functions to look for
            },
            files:{
                src:  [ '**/*.php' ], //Parse all php files
                expand: true,
            }
        },
        release: {
            files: [
                // makes all src relative to cwd
                { expand: true,
                    src: ['!node_modules/**/*', '!node_modules', '!*.md', '!*.json', '!Gruntfile.js'],
                    dest: 'build/deeper/'},
            ],
        },

        shell:{
            // Sync package.json version with git repo version
            updateVersion:{
                command: 'npm --version $(git describe --tags `git rev-list --tags --max-count=1`);'
            },
            // Increase package.json version one step
            bumpVersion: {
                command: 'npm --version patch'
            }
        }
    });
    // load task
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-release');
    grunt.loadNpmTasks('grunt-pot');
    // grunt.loadNpmTasks('grunt-shell');


    //register task
    // grunt.registerTask('default', ['shell']);
    grunt.registerTask('default', ['watch']);
    grunt.registerTask('build'  , ['clean', 'copy:release']); // builds production version in build folder
    grunt.registerTask('default', ['uglify']);
    grunt.registerTask('default', ['cssmin']);
};
