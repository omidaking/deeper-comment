﻿=== Deeper Comments ===
Contributors: webnus
Donate link: http://webnus.net/
Tags: comments, ajax comments , comment , flag comment , inappropriate comments , share comments , add instance comment
Requires at least: 4.0
Tested up to: 4.8.2
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Create awesome, full feature with modern design comments.

== Description ==

| Allow users and guest to delete own comments from front end (time-frame can be limited by admin). 
| Allow users to delete own comments from front end (time-frame can be limited by admin). 
| Allow guest to comment and vote. 
| Auto avatar generation with first letter of users and guest who has not gravatar set. 
| Quick login and register with smart spam protecting. 
| Users can edit their comments (time-frame can be limited by admin).
| Report system for marking inappropriate comments and automatically pending comment after defined time. 
| Adds interactive comment box on posts and other content types.
| Responsive comments form and comment threads design.
| Clean, simple and easy user interface and user experience.
| Comment list sorting by newest, oldest and most voted comments.
| Full integration with Social Network Login plugins (Google).
| Multi-level (nested) comment threads, with maximum levels depth setting option.
| Allows to create a new discussion thread and reply to existing comment.
| Fast and easy comment form with ajax validation and data submitting.
| Fully integrated and compatible with WordPress.
| Smart voting system with tracking by logged-in user and cookies.
| Post sharing options: Facebook, Twitter and Email.
| Option to overwrite comment template and style.
| Fully ajax and user friendly front end and back end.

== Screenshots ==
1. Logged in user ( Template one ).
2. Guest user ( Template one ).
3. Logged in user ( Template tow ).
4. Logged in user ( Template three ).
5. Logged in user ( Template four ).
6. Singup modal form for guest.
7. Back-end admin ( Comment settings ).
8. Back-end admin ( Inappropriate Comments ).
9. Back-end admin ( Social Share ).
10. Back-end admin ( Vote Option ).
11. Back-end admin ( Avatar ).
12. Back-end admin ( Login & Register ).
13. Back-end admin ( Google Login ).
14. Back-end admin ( Recaptcha ).
15. Back-end admin ( Comment Filter Bar ).
16. Back-end admin ( Load More ).

== Frequently Asked Questions ==


= Automatic installation (easiest way) =

log in to your WordPress dashboard, navigate to the Plugins menu and click Add New.

In the search field type "Deeper Comments" and click Search Plugins. Once you have found it you can install it by simply clicking "Install Now".


= Manual installation =

**Uploading in WordPress Dashboard**

1. Download `deeper-comments.zip`
2. Navigate to the 'Add New' in the plugins dashboard
3. Navigate to the 'Upload' area
4. Select `deeper-comments.zip` from your computer
5. Click 'Install Now'
6. Activate the plugin in the Plugin dashboard

**Using FTP**

1. Download `deeper-comments.zip`
2. Extract the `deeper-comments` directory to your computer
3. Upload the `deeper-comments` directory to the `/wp-content/plugins/` directory
4. Activate the plugin in the Plugin dashboard

The WordPress codex contains [instructions on how to install a WordPress plugin](http://codex.wordpress.org/Managing_Plugins#Manual_Plugin_Installation).



= Updating =

You can use automatic update to update the plugin safely.

= Minimum Requirements =
1. WordPress 4.2 or greater
2. PHP version 5.6.0 or greater
3. MySQL version 5.0 or greater


== Changelog ==

= 0.3.0 =
- Improve: Random color avatar for guests users.
- Improve: Load more button usability on filter options.
- Improve: Load more button usability on filter options.
- Fixed: Default data on plugin registration.
- Fixed: Font icon issues.


= 0.2.0 =
- Improve: Letter Avatars for speeding up comments.
- Improve: Clean coding.
- Fixed: Issue on filter bar for not logged in users.

= 0.1.0 =
- Improve: Load more options.
- Fixed: Issue on .

= 0.0.3 =
- Added: Screen shots and icons to assets.

= 0.0.2 =
- Fixed some minor issues.

= 0.0.1 =
- Initial version.

== Upgrade Notice ==

= 0.0.1 =
- Initial version.
