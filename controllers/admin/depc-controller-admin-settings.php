<?php

/**
 * @link       http://webnus.biz
 * @since      1.0.0
 *
 * @package    Depper Comment
 */

class Depc_Controller_Admin_Settings extends Depc_Controller_Admin {

	private $settings_api;

	const SETTINGS_PAGE_URL = Depc_Core::DEPC_ID;
	const REQUIRED_CAPABILITY = 'moderate_comments';


	/**
	 * Constructor
	 *
	 * @since    1.0.0
	 */
	protected function __construct() {

		$this->settings_api = Depc_Model_Admin_Settings::get_instance();
		add_action( 'admin_init', array($this, 'register_hook_callbacks') );
        add_action( 'admin_menu', array($this, 'admin_menu') );
        add_action( 'admin_menu', array($this, 'admin_submenu_menu') , 12 );

	}

	/**
	 * Register callbacks for actions and filters
	 *
	 * @since    1.0.0
	 */
	public function register_hook_callbacks() {
        //set the settings
        $this->settings_api->set_sections( $this->get_settings_sections() );
        $this->settings_api->set_fields( $this->get_settings_fields() );
        $this->settings_api->child_set_fields( $this->get_child_settings_fields() );
        //initialize settings
        $this->settings_api->admin_init();

	}

    public function admin_menu() {
        add_menu_page( 'Deeper Comments', 'Deeper', 'moderate_comments', 'deeper_intro' , array($this, 'hi') , 'dashicons-admin-comments' );
    }	

    public function admin_submenu_menu() {

        add_submenu_page( 'deeper_intro', 'Deeper Comment', 'Settings', 'moderate_comments', 'deeper_setting', array($this, 'plugin_page') );
        remove_submenu_page('deeper_intro','deeper_intro');
    }

    public function hi() { echo "string";}

	public function get_settings_sections() {
		$sections = array(
			// array(
			// 	'id'        => 'general_setting',
   //              'title'     => __( 'General Settings', 'depc' ),
   //              'icon'  => 'sl-social-twitter'
   //              // 'submenu'   => array(
   //              //     'id' => __( 'Comment_Link_option', 'depc' ),
   //              //     'id1' => __( 'Flag_and_report_option', 'depc' )
   //              //     )
   //              ),
            array(
				'id'    => 'disscustion_settings',
				'title' => __( 'Discussion Settings', 'depc' ),
				'icon'  => 'sl-speech',
				'submenu'   => array(
				    'id'  => __( 'Comment_Settings', 'depc' ),
				    'id1' => __( 'Inappropriate_Comments', 'depc' ),
				    'id2' => __( 'Social_Share', 'depc' ),
                    'id3' => __( 'Vote_Options', 'depc' ),
				    'id4' => __( 'Avatar', 'depc' )
				    )
				),	            
            array(
				'id'    => 'social_login',
				'title' => __( 'Login Register', 'depc' ),
				'icon'  => 'sl-login',
				'submenu'   => array(
				    'id'    => __( 'Login_Register', 'depc' ),
                    'id1'   => __( 'Google', 'depc' ),
				    'id2'   => __( 'Recaptcha', 'depc' )
					)
				),	
			array(
				'id'		=> 'Comments_Filter',
				'title'		=> __( 'Comments Filter & Search', 'depc' ),
				'icon'		=> 'sl-magnifier',
				'submenu'   => array(
                    'id' => __( 'Comment_Filter_Bar', 'depc' ),
					'id1' => __( 'Load_More', 'depc' ),
				)
			),		
            // array(
            //     'id'    => 'subscription',
            //     'title' => __( 'Subscription', 'depc' ),
            //     'icon'  => 'sl-heart',
            //     // 'submenu'   => array(
            //     //     'id' => __( 'dddd_ff_s', 'depc' ),
            //     //     )
            //     )           
            // array(
            //     'id'    => 'integration',
            //     'title' => __( 'Integration', 'depc' ),
            //     'icon'  => 'sl-social-twitter',
            //     // 'submenu'   => array(
            //     //     'id' => __( 'dddd_ff_s', 'depc' ),
            //     //     )
            //     ),            
            // array(
            //     'id'    => 'tools',
            //     'title' => __( 'Tools', 'depc' ),
            //     'icon'  => 'sl-social-twitter',
            //     // 'submenu'   => array(
            //     //     'id' => __( 'dddd_ff_s', 'depc' ),
            //     //     )
            //     ),            
            // array(
            //     'id'    => 'form_builder',
            //     'title' => __( 'Form builder', 'depc' ),
            //     'icon'  => 'sl-social-twitter',
            //     // 'submenu'   => array(
            //     //     'id' => __( 'dddd_ff_s', 'depc' ),
            //     //     )
            //     ),            
            // array(
            //     'id'    => 'phrases',
            //     'title' => __( 'Phrases', 'depc' ),
            //     'icon'  => 'sl-social-twitter',
            //     // 'submenu'   => array(
            //     //     'id' => __( 'dddd_ff_s', 'depc' ),
            //     //     )
            //     )

			);
		return $sections;
	}

    /**
     * Returns all the settings fields
     *
     * @return array settings fields
     */
    function get_settings_fields() {
    	$settings_fields = array(
    		// 'general_setting' => array(
      //           array(
      //               'name'              => 'text_val',
      //               'label'             => __( 'Text Input', 'depc' ),
      //               'desc'              => __( 'Text input description', 'depc' ),
      //               'type'              => 'color',
      //               'default'           => 'Title',
      //               'sanitize_callback' => 'sanitize_text_field'
      //               ),
      //           array(
      //               'name'              => 'switch',
      //               'label'             => __( 'switch Input', 'depc' ),
      //               'type'              => 'switch',
      //               'default'           => 'Title',
      //               'sanitize_callback' => 'sanitize_text_field'
      //               ),
    		// 	array(
    		// 		'name'              => 'number_input',
    		// 		'label'             => __( 'Number Input', 'depc' ),
    		// 		'desc'              => __( 'Number field with validation callback `floatval`', 'depc' ),
    		// 		'placeholder'       => __( '1.99', 'depc' ),
    		// 		'min'               => 0,
    		// 		'max'               => 100,
    		// 		'step'              => '0.01',
    		// 		'type'              => 'number',
    		// 		'default'           => 'Title',
    		// 		'sanitize_callback' => 'floatval'
    		// 		),
    		// 	array(
    		// 		'name'        => 'textarea',
    		// 		'label'       => __( 'Textarea Input', 'depc' ),
    		// 		'desc'        => __( 'Textarea description', 'depc' ),
    		// 		'placeholder' => __( 'Textarea placeholder', 'depc' ),
    		// 		'type'        => 'textarea'
    		// 		),
    		// 	array(
    		// 		'name'        => 'html',
    		// 		'desc'        => __( 'HTML area description. You can use any <strong>bold</strong> or other HTML elements.', 'depc' ),
    		// 		'type'        => 'html'
    		// 		),
    		// 	array(
    		// 		'name'  => 'checkbox',
    		// 		'label' => __( 'Checkbox', 'depc' ),
    		// 		'desc'  => __( 'Checkbox Label', 'depc' ),
    		// 		'type'  => 'checkbox'
    		// 		),
    		// 	array(
    		// 		'name'    => 'radio',
    		// 		'label'   => __( 'Radio Button', 'depc' ),
    		// 		'desc'    => __( 'A radio button', 'depc' ),
    		// 		'type'    => 'radio',
    		// 		'options' => array(
    		// 			'yes' => 'Yes',
    		// 			'no'  => 'No'
    		// 			)
    		// 		),
    		// 	array(
    		// 		'name'    => 'selectbox',
    		// 		'label'   => __( 'A Dropdown', 'depc' ),
    		// 		'desc'    => __( 'Dropdown description', 'depc' ),
    		// 		'type'    => 'select',
    		// 		'default' => 'no',
    		// 		'options' => array(
    		// 			'yes' => 'Yes',
    		// 			'no'  => 'No'
    		// 			)
    		// 		),
    		// 	array(
    		// 		'name'    => 'password',
    		// 		'label'   => __( 'Password', 'depc' ),
    		// 		'desc'    => __( 'Password description', 'depc' ),
    		// 		'type'    => 'password',
    		// 		'default' => ''
    		// 		),
    		// 	array(
    		// 		'name'    => 'file',
    		// 		'label'   => __( 'File', 'depc' ),
    		// 		'desc'    => __( 'File description', 'depc' ),
    		// 		'type'    => 'file',
    		// 		'default' => '',
    		// 		'options' => array(
    		// 			'button_label' => 'Choose Image'
    		// 			)
    		// 		)
    		// 	),
    		'disscustion_settings' => array(
                // array(
                //     'name'    => 'dc_fb_login_enable',
                //     'label'   => __( 'Socials login FB', 'depc' ),
                //     'desc'    => __( 'Socials login FB', 'depc' ),
                //     'type'    => 'switch',
                //     'default' => 'off'
                // ),
                // array(
                //     'name'    => 'dc_tw_login_enable',
                //     'label'   => __( 'Socials login Twitter', 'depc' ),
                //     'desc'    => __( 'Socials login Twitter', 'depc' ),
                //     'type'    => 'switch',
                //     'default' => 'off'
                // ),             

                // array(
                //     'name'    => 'dc_loading',
                //     'label'   => __( 'Discussions loading/pagination type', 'depc' ),
                //     'desc'    => __( 'Discussions loading/pagination type', 'depc' ),
                //     'type'    => 'select',
                //     'default' => 'lazy',
                //     'options' => array(
                //         'load_more' => 'Load more Button',
                //         'load_rest' => '[Load rest of all Discussions] Button',
                //         'lazy'      => 'Lazy load Discussions on scrolling '
                //     )
                // ),
                // array(
                //     'name'    => 'dc_date_time',
                //     'label'   => __( 'Use WordPress Date/Time', 'depc' ),
                //     'desc'    => __( 'Use WordPress Date/Time format', 'depc' ),
                //     'type'    => 'radio',
                //     'options' => array(
                //         'yes' => 'Yes',
                //         'no'  => 'No'
                //     )
                // ),
                // array(
                //     'name'    => 'dc_live_update',
                //     'label'   => __( 'Live update', 'depc' ),
                //     'desc'    => __( 'Always check for new Discussions and update automatically', 'depc' ),
                //     'type'    => 'radio',
                //     'options' => array(
                //         'yes' => 'Yes',
                //         'no'  => 'No'
                //         )
                // ),
                // array(
                //     'name'    => 'dc_show_loggedin',
                //     'label'   => __( 'Show logged-in user ', 'depc' ),
                //     'desc'    => __( 'Show logged-in user name and logout link on top of main form', 'depc' ),
                //     'type'    => 'checkbox'
                // ),
                // array(
                //     'name'    => 'dc_label',
                //     'label'   => __( 'Hide Discussioner Labels', 'depc' ),
                //     'desc'    => __( 'Hide Discussioner Labels', 'depc' ),
                //     'type'    => 'checkbox'
                // ),
                // array(
                //     'name'    => 'dc_img_url',
                //     'label'   => __( 'Image URL Labels', 'depc' ),
                //     'desc'    => __( 'Enable automatic image URL to image HTML conversion', 'depc' ),
                //     'type'    => 'checkbox'
                // ),
                // array(
                //     'name'    => 'dc_profile_url',
                //     'label'   => __( 'Disable Profiles URL', 'depc' ),
                //     // 'desc'    => __( 'Enable automatic image URL to image HTML conversion', 'depc' ),
                //     'type'    => 'checkbox'
                // ),		    

		    ),
            'social_login' => array(
            ),            
            'Comments_Filter' => array(
            ),
            // 'subscription' => array(
            //     array(
            //         'name'    => 'dc_subs_confrmation_registred',
            //         'label'   => __( 'Disable subscription confirmation', 'depc' ),
            //         'desc'    => __( 'Disable subscription confirmation for registered users', 'depc' ),
            //         'type'    => 'checkbox'
            //     ),
            //     array(
            //         'name'    => 'dc_subs_confrmation_guest',
            //         'label'   => __( 'Disable subscription confirmation', 'depc' ),
            //         'desc'    => __( 'Disable subscription confirmation for guest', 'depc' ),
            //         'type'    => 'checkbox'
            //     ),
            // ),
            // 'styling' => array(
            //     array(
            //         'name'              => 'dc_primary',
            //         'label'             => __( 'Primary Color', 'depc' ),
            //         'desc'              => __( 'Primary Color', 'depc' ),
            //         'type'              => 'color',
            //         // 'default'           => 'Title',
            //         'sanitize_callback' => 'sanitize_text_field'
            //     ),
            //     array(
            //         'name'              => 'dc_label_color_admin',
            //         'label'             => __( 'Label color', 'depc' ),
            //         'desc'              => __( 'Administrator label color', 'depc' ),
            //         'type'              => 'color',
            //         // 'default'           => 'Title',
            //         'sanitize_callback' => 'sanitize_text_field'
            //     ),
            //     array(
            //         'name'              => 'dc_label_color_editor',
            //         'label'             => __( 'Label color', 'depc' ),
            //         'desc'              => __( 'Editor label color', 'depc' ),
            //         'type'              => 'color',
            //         // 'default'           => 'Title',
            //         'sanitize_callback' => 'sanitize_text_field'
            //     ),
            //     array(
            //         'name'              => 'dc_label_color_athur',
            //         'label'             => __( 'Label color', 'depc' ),
            //         'desc'              => __( 'Athur label color', 'depc' ),
            //         'type'              => 'color',
            //         // 'default'           => 'Title',
            //         'sanitize_callback' => 'sanitize_text_field'
            //     ),
            //     array(
            //         'name'              => 'dc_label_color_contrib',
            //         'label'             => __( 'Label color', 'depc' ),
            //         'desc'              => __( 'Contributor label color', 'depc' ),
            //         'type'              => 'color',
            //         // 'default'           => 'Title',
            //         'sanitize_callback' => 'sanitize_text_field'
            //     ),
            //     array(
            //         'name'              => 'dc_label_color_contrib',
            //         'label'             => __( 'Label color', 'depc' ),
            //         'desc'              => __( 'Contributor label color', 'depc' ),
            //         'type'              => 'color',
            //         // 'default'           => 'Title',
            //         'sanitize_callback' => 'sanitize_text_field'
            //     ),
            //     array(
            //         'name'              => 'dc_label_color_guest',
            //         'label'             => __( 'Label color', 'depc' ),
            //         'desc'              => __( 'Guest label color', 'depc' ),
            //         'type'              => 'color',
            //         // 'default'           => 'Title',
            //         'sanitize_callback' => 'sanitize_text_field'
            //     ),
            // 'integration' => array(
            //     array(
            //         'name'    => 'dc_wp_social_login',
            //         'label'   => __( 'WordPress Social Login', 'depc' ),
            //         // 'desc'    => __( '21-   Captcha generation type', 'depc' ),
            //         'type'    => 'radio',
            //         'options' => array(
            //             'yes' => 'Yes',
            //             'no'  => 'No'
            //             )
            //     ),
            //     array(
            //         'name'    => 'dc_social_login',
            //         'label'   => __( 'Social Login', 'depc' ),
            //         // 'desc'    => __( '21-   Captcha generation type', 'depc' ),
            //         'type'    => 'radio',
            //         'options' => array(
            //             'yes' => 'Yes',
            //             'no'  => 'No'
            //             )
            //     ),
            //     array(
            //         'name'    => 'dc_super_social',
            //         'label'   => __( 'Super Socializer', 'depc' ),
            //         // 'desc'    => __( '21-   Captcha generation type', 'depc' ),
            //         'type'    => 'radio',
            //         'options' => array(
            //             'yes' => 'Yes',
            //             'no'  => 'No'
            //             )
            //     ),
            //     array(
            //         'name'    => 'dc_socia_connect',
            //         'label'   => __( 'Social Connect', 'depc' ),
            //         // 'desc'    => __( '21-   Captcha generation type', 'depc' ),
            //         'type'    => 'radio',
            //         'options' => array(
            //             'yes' => 'Yes',
            //             'no'  => 'No'
            //             )
            //     ),
            //     array(
            //         'name'    => 'dc_user_ltra',
            //         'label'   => __( 'User Ultra', 'depc' ),
            //         // 'desc'    => __( '21-   Captcha generation type', 'depc' ),
            //         'type'    => 'radio',
            //         'options' => array(
            //             'yes' => 'Yes',
            //             'no'  => 'No'
            //             )
            //     ),
            //     array(
            //         'name'    => 'dc_userpro',
            //         'label'   => __( 'UserPro', 'depc' ),
            //         // 'desc'    => __( '21-   Captcha generation type', 'depc' ),
            //         'type'    => 'radio',
            //         'options' => array(
            //             'yes' => 'Yes',
            //             'no'  => 'No'
            //             )
            //     ),
            //     array(
            //         'name'    => 'dc_ultimate_member',
            //         'label'   => __( 'Ultimate Member', 'depc' ),
            //         // 'desc'    => __( '21-   Captcha generation type', 'depc' ),
            //         'type'    => 'radio',
            //         'options' => array(
            //             'yes' => 'Yes',
            //             'no'  => 'No'
            //             )
            //     ),
            //     array(
            //         'name'    => 'dc_mycred',
            //         'label'   => __( 'Mycred', 'depc' ),
            //         // 'desc'    => __( '21-   Captcha generation type', 'depc' ),
            //         'type'    => 'radio',
            //         'options' => array(
            //             'yes' => 'Yes',
            //             'no'  => 'No'
            //             )
            //     ),
            // )
	    );

    	return $settings_fields;
    }

    /**
     * Returns all the settings fields
     *
     * @return array settings fields
     */
    function get_child_settings_fields() {
        $settings_fields = array(           
			'Comment_Settings' => array(
				array(
					'name'    => 'dc_defultcnt_comment',
					'label'   => __( 'Number of comment to demonstrate', 'depc' ),
					'type'    => 'text',
                    'default' => '15'
				),    
				array(
					'name'    => 'dc_delete_member',
					'label'   => __( 'Allow delete Comment to members', 'depc' ),
					'desc'    => __( 'Allow delete comment in front-end for logged in users', 'depc' ),
					'type'    => 'switch',
					'default' => 'on'
				),				
				array(
					'name'    => 'dc_delete_guest',
					'label'   => __( 'Allow delete Comment to guest', 'depc' ),
					'desc'    => __( 'Allow delete comment in front-end for guest', 'depc' ),
					'type'    => 'switch',
					'default' => 'on'
				),				
				array(
					'name'    => 'dc_edit_member',
					'label'   => __( 'Allow edit Comment for members', 'depc' ),
					'desc'    => __( 'Allow edit comment in front-end for logged in users', 'depc' ),
					'type'    => 'switch',
					'default' => 'on'
				),				
				array(
					'name'    => 'dc_edit_guest',
					'label'   => __( 'Allow edit Comment for guest', 'depc' ),
					'desc'    => __( 'Allow edit comment in front-end for guest', 'depc' ),
					'type'    => 'switch',
					'default' => 'on'
				),
                array(
                    'name'              => 'dc_edit_expiration',
                    'label'             => __( 'Edit comment time', 'depc' ),
                    'desc'              => __( 'Set maximum day for comment submitter can edit 1(day) to unlimited(days)', 'depc' ),
                    'placeholder'       => __( '1 to unlimited', 'depc' ),
                    'min'               => 0,
                    'step'              => '1',
                    'type'              => 'number',
                    'default'           => '10',
                    'sanitize_callback' => 'floatval'
                ),
				array(
					'name'    => 'dc_link_member',
					'label'   => __( 'Comment Link for members', 'depc' ),
					'desc'    => __( 'Show or hide comment link button for logged in users', 'depc' ),
					'type'    => 'switch',
					'default' => 'on'
				),				
				array(
					'name'    => 'dc_link_guest',
					'label'   => __( 'Comment Link for guest', 'depc' ),
					'desc'    => __( 'Show or hide comment link button for guest', 'depc' ),
					'type'    => 'switch',
					'default' => 'on'
				),				
				array(
					'name'    => 'dc_collapse_member',
					'label'   => __( 'Comment collapse for members', 'depc' ),
					'desc'    => __( 'Show or hide comment collapse button for logged in users', 'depc' ),
					'type'    => 'switch',
					'default' => 'on'
				),				
				array(
					'name'    => 'dc_collapse_guest',
					'label'   => __( 'Comment collapse for guest', 'depc' ),
					'desc'    => __( 'Show or hide comment collapse button for guest', 'depc' ),
					'type'    => 'switch',
					'default' => 'on'
				),
				array(
					'name'    => 'dc_allow_guest_comment',
					'label'   => __( 'Allow Guest Comment', 'depc' ),
					'desc'    => __( 'If set to on deeper would allow guests to comment.', 'depc' ),
					'type'    => 'switch',
					'default' => 'on'
				),
                // array(
                //     'name'    => 'dc_filter',
                //     'label'   => __( 'Discussions Filter', 'depc' ),
                //     'desc'    => __( 'Discussions Filter ( Trending,  Popular, Oldest, Newest, Picked )', 'depc' ),
                //     'type'    => 'radio',
                //     'options' => array(
                //         'yes' => 'Yes',
                //         'no'  => 'No'
                //     )
                // ),
                // array(
                //     'name'    => 'dc_readmore',
                //     'label'   => __( 'Read more', 'depc' ),
                //     'desc'    => __( 'The number of words before breaking Discussion text and showing "Read more" link', 'depc' ),
                //     'type'    => 'number'
                // ),
                // array(
                //     'name'    => 'dc_txt_size',
                //     'label'   => __( 'Discussion text size', 'depc' ),
                //     'desc'    => __( 'Discussion text size in pixels', 'depc' ),
                //     'type'    => 'number'
                // ),
			),
			'Inappropriate_Comments' => array(
				array(
				    'name'    => 'dc_inappropriate_members',
				    'label'   => __( 'Inappropriate flag for member', 'depc' ),
				    'desc'    => __( 'Show or hide inappropriate comments flag on top of discussions', 'depc' ),
				    'type'    => 'switch',
				    'default' => 'on'
				),                
				array(
					'name'    => 'dc_inappropriate_guest',
					'label'   => __( 'Inappropriate flag for guest', 'depc' ),
					'desc'    => __( 'Show or hide inappropriate comments flag on top of discussions', 'depc' ),
					'type'    => 'switch',
					'default' => 'on',
				),                
				array(
					'name'    => 'dc_inapp_auto_ban',
					'label'   => __( 'Automatic pending comments', 'depc' ),
					'desc'    => __( 'How many report needs to pending comments automatically', 'depc' ),
					'type'    => 'number',
					'default' => '5',
				),
			),
			'Social_Share' => array(
				array(
					'name'    => 'dc_social_enable',
					'label'   => __( 'Social share', 'depc' ),
					'desc'    => __( 'Show social share menu on comments', 'depc' ),
					'type'    => 'switch',
					'default' => 'on'
				),              
				array(
					'name'    => 'dc_social_share_fb',
					'label'   => __( 'Facebook share', 'depc' ),
					'desc'    => __( 'Show or hide facebook share button', 'depc' ),
					'type'    => 'switch',
					'default' => 'on'
				),              
				array(
					'name'    => 'dc_social_share_tw',
					'label'   => __( 'Twitter share', 'depc' ),
					'desc'    => __( 'Show or hide facebook share button', 'depc' ),
					'type'    => 'switch',
					'default' => 'on'
				),              
				array(
					'name'    => 'dc_social_share_mail',
					'label'   => __( 'Share by mail', 'depc' ),
					'desc'    => __( 'Show or hide mail share button', 'depc' ),
					'type'    => 'switch',
					'default' => 'on'
				),
			),
            'Vote_Options' => array(
                array(
                    'name'    => 'dc_vote_user_enable',
                    'label'   => __( 'User Comment Like/Dislike', 'depc' ),
                    'desc'    => __( 'Enable comment Like/Dislike for logged in users', 'depc' ),
                    'type'    => 'switch',
                    'default' => 'on'
                ),                      
                array(
                    'name'              => 'dc_vote_user_date',
                    'label'             => __( 'Users voting limitation time', 'depc' ),
                    'desc'              => __( 'Set limit to user Like/Dislike accept value from 1(day) to unlimited(days)', 'depc' ),
                    'placeholder'       => __( '1 to unlimited', 'depc' ),
                    'min'               => 0,
                    'step'              => '1',
                    'type'              => 'number',
                    'default'           => '10',
                    'sanitize_callback' => 'floatval'
                ),
                array(
                    'name'    => 'dc_vote_guest_enable',
                    'label'   => __( 'Guest Comment Like/Dislike', 'depc' ),
                    'desc'    => __( 'Enable comment Like/Dislike for not logged-in users', 'depc' ),
                    'type'    => 'switch',
                    'default' => 'on'
                ),
                array(
                    'name'              => 'dc_vote_guest_date',
                    'label'             => __( 'Guest voting limitation time', 'depc' ),
                    'desc'              => __( 'Set limit for not logged-in users Like/Dislike accept value from 1(day) to unlimited(days)', 'depc' ),
                    'placeholder'       => __( '1 to unlimited', 'depc' ),
                    'min'               => 0,
                    'step'              => '1',
                    'type'              => 'number',
                    'default'           => '10',
                    'sanitize_callback' => 'floatval'
                ),
			),			
			'Avatar' => array(
				array(
					'name'    => 'dc_generate_avatar',
					'label'   => __( 'Genrate Avatar', 'depc' ),
					'desc'    => __( 'If on deeper will be make avatar (For who has not gravatar) with first word of user name if off wordpress setting will be utlize.', 'depc' ),
					'type'    => 'switch',
					'default'  => 'on'
				),                
				// array(
				// 	'name'    => 'dc_generate_random_color',
				// 	'label'   => __( 'Genrate Avatar', 'depc' ),
				// 	'desc'    => __( 'If on deeper will be make avatar (For who has not gravatar and Dose not set his/her color in profile setting yet) with random color each time.', 'depc' ),
				// 	'type'    => 'switch',
				// 	'default'  => 'on'
				// ),
			),
			'Login_Register' => array(
				array(
					'name'    => 'dc_quick_login',
					'label'   => __( 'Quick Login', 'depc' ),
					'desc'    => __( 'Quick login discussions for not logged in users', 'depc' ),
					'type'    => 'switch',
					'default' => 'on'
				),				
				array(
					'name'    => 'dc_quick_register',
					'label'   => __( 'Quick Register', 'depc' ),
					'desc'    => __( 'Quick register discussions for not logged in users', 'depc' ),
					'type'    => 'switch',
					'default' => 'on'
				),
			),		
            'Google' => array(
                array(
                    'name'    => 'dc_google_login_enable',
                    'label'   => __( 'Enable google login', 'depc' ),
                    'desc'    => __( 'Enable google login for not logged in users.', 'depc' ),
                    'type'    => 'switch',
                    'default' => 'on'
                    ),
                array(
                    'name'    => 'dc_social_login_g_appname',
                    'label'   => __( 'Google Client App Name', 'depc' ),
                    'type'    => 'text'                
                ),                 
                array(
                    'name'    => 'dc_social_login_g_client',
                    'label'   => __( 'Google Client ID', 'depc' ),
                    'desc'    => __( 'Must look like 253673258632-j3omdk62ad4l7069u56ufs20tjbp202d.apps.googleusercontent.com', 'depc' ),
                    'type'    => 'text'                
                ),                
                array(
                    'name'    => 'dc_social_login_g_secret',
                    'label'   => __( 'Google Client Secret', 'depc' ),
                    'desc'    => __( 'Must look like JgNAlfdK2RTvAc-32Bwz_8jf', 'depc' ),
                    'type'    => 'text'
                ),
            ),			
            'Recaptcha' => array(
				array(
					'name'    => 'dc_recaptcha_type',
					'label'   => __( 'Captcha generation', 'depc' ),
					'desc'    => __( 'Captcha generation type.', 'depc' ),
					'type'    => 'select',
					'options' => array(
						'none'     => 'Dont use Recaptcha',
						'google'   => 'Google Recaptcha'
					)
				),
				array(
					'name'    => 'dc_recptcha_gsitekey',
					'label'   => __( 'Site key', 'depc' ),
					'type'    => 'text'
				),
				array(
					'name'    => 'dc_recptcha_gsecretkey',
					'label'   => __( 'Secret key', 'depc' ),
					'type'    => 'text'                
				),                
				array(
					'name'    => 'dc_recaptcha_addcm',
					'label'   => __( 'Enable for comments (guest)', 'depc' ),
					'desc'    => __( 'If enabled recaptcha whould be show in comment submission and replies for not logged in users.', 'depc' ),
					'type'    => 'switch'
				),			
				array(
					'name'    => 'dc_recaptcha_theme',
					'label'   => __( 'Google captcha theme', 'depc' ),
					'type'    => 'select',
					'default' => 'light',
					'options' => array(
						'dark'     => 'Dark',
						'light'    => 'Light'
					)
				),					
				array(
					'name'    => 'dc_recaptcha_size',
					'label'   => __( 'Google captcha size', 'depc' ),
					'type'    => 'select',
					'default' => 'normal',
					'options' => array(
						'compact'  => 'Compact',
						'normal'   => 'Normal'
					)
				),			
			),
            'Comment_Filter_Bar' => array(
                array(
                    'name'    => 'dc_enable_filter_member',
                    'label'   => __( 'Enable Filter Option (Member)', 'depc' ),
                    'desc'    => __( 'If enabled filter bar will show on top of the comments for logged in users.', 'depc' ),
                    'type'    => 'switch'
                ),                  
                array(
                    'name'    => 'dc_enable_filter_guest',
                    'label'   => __( 'Enable Filter Option (Guest)', 'depc' ),
                    'desc'    => __( 'If enabled filter bar will show on top of the comments for not logged in users.', 'depc' ),
                    'type'    => 'switch'
                ),              
                array(
                    'name'    => 'dc_default_sorting',
                    'label'   => __( 'Default Comment Filter', 'depc' ),
                    'type'    => 'select',
                    'default' => 'newest',
                    'options' => array(
                        // 'trending'  => 'Trending',
                        // 'popular'   => 'Popular',
                        'oldest'   => 'Oldest',
                        'newest'   => 'Newest'
                    )
                ),
                // array(
                //  'name'    => 'dc_enable_trending_day',
                //  'label'   => __( 'Select Trending Day', 'depc' ),
                //  'desc'    => __( 'Evaluate day for comments to chosen for trending date.', 'depc' ),
                //  'type'    => 'number',
                //  'placeholder'       => __( '1', 'depc' ),
                //  'min'               => 0,
                //  'max'               => 100,
                //  'step'              => '1',
                //  'type'              => 'number',
                //  'default'           => '1',
                //  'sanitize_callback' => 'number'
                // ),           
            ),			
			'Load_More' => array(
				array(
					'name'	  => 'dc_enable_loadmore',
					'label'	  => __( 'Enable load more button', 'depc' ),
					'desc' 	  => __( 'If off load more button wont demonstrate to anybody.', 'depc' ),
					'type' 	  => 'switch'
				),								
				array(
					'name'    => 'dc_enable_loadmore_count',
					'label'   => __( 'Number of comments to load', 'depc' ),
					'desc'    => __( 'Number of comments loads when load more button clicked.', 'depc' ),
					'type'    => 'number',
					'placeholder'       => __( '5', 'depc' ),
					'min'               => 1,
					'max'               => 20,
					'step'              => '1',
					'default'           => '5',
					'sanitize_callback' => 'number'
                ),     
			),
        );

        return $settings_fields;
    }

    function plugin_page() {
    	echo ' 
            <div id="wrap">
                <div class="container">
                <hr class="vertical-space5">
                    <div class="dpr-be-container">';

                	$this->settings_api->show_navigation();
                	$this->settings_api->show_forms();

    	echo '       </div>
                </div>
            </div>';
    }

    /**
     * Get all the pages
     *
     * @return array page names with key value pairs
     */
    function get_pages() {
    	$pages = get_pages();
    	$pages_options = array();
    	if ( $pages ) {
    		foreach ($pages as $page) {
    			$pages_options[$page->ID] = $page->post_title;
    		}
    	}

    	return $pages_options;
    }

	/**
	 * Creates the markup for the Settings page
	 *
	 * @since    1.0.0
	 */
	public function markup_settings_page() {

		if ( current_user_can( static::REQUIRED_CAPABILITY ) ) {

			echo static::render_template(
				'page-settings/page-settings.php',
				array(
					'page_title' 	=> Depc_Core::DEPC_NAME,
					'settings_name' => Depc_Model_Admin_Settings::SETTINGS_NAME
					)
				);

		} else {

			wp_die( __( 'Access denied.' ) );

		}

	}

	/**
	 * Adds the section introduction text to the Settings page
	 *
	 * @param array $section
	 *
	 * @since    1.0.0
	 */
	public function markup_section_headers( $section ) {

		echo static::render_template(
			'page-settings/page-settings-section-headers.php',
			array(
				'section'      => $section,
				'text_example' => __( 'This is a text example for section header',Depc_Core::DEPC_ID )
				)
			);

	}

	/**
	 * Delivers the markup for settings fields
	 *
	 * @param array $args
	 *
	 * @since    1.0.0
	 */
	public function markup_fields( $field_args ) {

		$field_id = $field_args['id'];
		$settings_value = static::get_model()->get_settings( $field_id );

		echo static::render_template(
			'page-settings/page-settings-fields.php',
			array(
				'field_id'       => esc_attr( $field_id ),
				'settings_name'  => Depc_Model_Admin_Settings::SETTINGS_NAME,
				'settings_value' => ! empty( $settings_value ) ? esc_attr( $settings_value ) : ''
				),
			'always'
			);

	}

}
