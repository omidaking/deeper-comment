<?php

/**
 * @link       http://webnus.biz
 * @since      1.0.0
 *
 * @package    Depper Comment
 */

class Depc_Controller_Public extends Depc_Controller {

	private $recaptcha;

	/**
	 * Constructor
	 *
	 * @since    1.0.0
	 */
	protected function __construct() {

		$this->register_hook_callbacks();
	}

	/**
	 * Register callbacks for actions and filters
	 *
	 * @since    1.0.0
	 */
	protected function register_hook_callbacks() {
		// set scripts and styles
		Depc_Actions_Filters::add_action( 'wp_enqueue_scripts', $this, 'enqueue_styles', 9999 );
		Depc_Actions_Filters::add_action( 'wp_enqueue_scripts', $this, 'enqueue_scripts' );
		Depc_Actions_Filters::add_filter( 'comments_template',  $this, 'comments_template' );

		// get user setting
		$this->recaptcha['recaptcha'] = Depc_Core::get_option( 'dc_recaptcha_type', 'Recaptcha' );
		$this->recaptcha['recptcha_gsitekey']  = Depc_Core::get_option( 'dc_recptcha_gsitekey', 'Recaptcha' );
		$this->recaptcha['captcha_theme']  = Depc_Core::get_option( 'dc_recaptcha_theme', 'Recaptcha' );
		$this->recaptcha['captcha_size']  = Depc_Core::get_option( 'dc_recaptcha_size', 'Recaptcha' );


	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		wp_enqueue_style(
			Depc_Core::DEPC_ID,
			Depc_Core::get_depc_url() . 'views/css/deeper.min.css',
			array(),
			Depc_Core::DEPC_VERSION,
			'all'
		);
		wp_enqueue_style(
			'deeper-icon',
			Depc_Core::get_depc_url() . 'views/css/package/iconfonts.css',
			array(),
			Depc_Core::DEPC_VERSION,
			'all'
		);

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		wp_enqueue_script(
			'dpr_tinymce_js',
			includes_url( 'js/tinymce/' ) . 'wp-tinymce.php',
			array( 'jquery' ),
			false,
			false
		);

		wp_enqueue_script(
			Depc_Core::DEPC_ID,
			Depc_Core::get_depc_url() . 'views/js/deeper.min.js',
			array( 'jquery' ),
			Depc_Core::DEPC_VERSION,
			false
		);	

		// enque recaptcha if needed	
		if ( $this->recaptcha['recaptcha'] == 'google' ) {
			$locale = $this->get_current_language();
			wp_enqueue_script(
				Depc_Core::DEPC_ID . 'recaptcha',
				'//www.google.com/recaptcha/api.js?hl='.str_replace('_', '-', $locale),
				array( 'jquery' ),
				Depc_Core::DEPC_VERSION,
				false
			);
		}
		
		$this->inline();
		$this->loclize();
	}

	/**
	 * Show defult comment template
	 *
	 * @since    1.0.0
	 */
	public function comments_template(){

		return Depc_Core::get_depc_path() . '/views/tpl/tpl.php';

	}

	/**
	 * Define some loclization to js.
	 *
	 * @since    1.0.0
	 */
	private function loclize(){

		wp_localize_script( Depc_Core::DEPC_ID , 'dpr', array(
			'editor_title'      => __( 'Add Your Comment' , 'depc' ),
			'post_comment'      => __( 'Submit' , 'depc' ),
			'cancle'            => __( 'Cancle' , 'depc' ),
			'ok'           		=> __( 'ok' , 'depc' ),
			'duplicate'         => __( 'Duplicate Comment!' , 'depc' ),
			'save_comment'      => __( 'Save' , 'depc' ),
			'empty'     		=> __( 'Please fill your comment.' , 'depc' ),
			'sure_delete'     	=> __( 'Are you sure for delete this comment?' , 'depc' ),
			'delete_cm'     	=> __( 'Delete Comment' , 'depc' ),
			'delete'     		=> __( 'Delete' , 'depc' ),
			'name'     			=> __( 'Name' , 'depc' ),
			'email'     		=> __( 'Email' , 'depc' ),
			'username'     		=> __( 'User Name' , 'depc' ),
			'password'     		=> __( 'Password' , 'depc' ),
			'confirm_password'  => __( 'Password' , 'depc' ),
			'login'     		=> __( 'Login' , 'depc' ),
			'lost_pass'         => __( 'Forgot password?' , 'depc' ),
			'singup'         	=> __( 'Singup' , 'depc' ),
			'flag_cm'         	=> __( 'Inappropriate Comment' , 'depc' ),
			'sure_flag'         => __( 'Are you sure to mark this comment as inappropriate?' , 'depc' ),
			'lost_url'          => wp_lostpassword_url(),
			'adminajax'         => admin_url( 'admin-ajax.php' ),
			'rtl'				=> is_rtl(),
			'recaptcha'			=> $this->recaptcha['recptcha_gsitekey'],
			'recaptcha_theme'	=> $this->recaptcha['captcha_theme'],
			'recaptcha_size'	=> $this->recaptcha['captcha_size'],
		));

	}	

	/**
	 * Add inline styles.
	 *
	 * @since    1.0.0
	 */
	private function inline(){
		// get vote inline script
		$vote = Depc_Model_Public_Comment_Vote::get_instance();
		$javascript = $vote::scripts();

		// get comment inline script
		$comment = Depc_Controller_Public_Comment::get_instance();
		$javascript .= $comment->scripts();
		
		// get filter comment inline script
		$filter = Depc_Controller_Public_Comment_Filter::get_instance();
		$javascript .= $filter->scripts();		

		// get load more script
		$filter = Depc_Controller_Public_Comment_Loadmore::get_instance();
		$javascript .= $filter->scripts();

		// make filter for inline script
		apply_filters( 'depc_inline_comment_script', $javascript );
		// add scripts
		wp_add_inline_script( Depc_Core::DEPC_ID , $javascript, 'after' );

	}

	/**
	 * Get current language of WordPress
	 * @author Webnus <info@webnus.biz>
	 * @return string
	 */
	public function get_current_language() {
		return apply_filters('plugin_locale', get_locale(), 'depc');
	}

}