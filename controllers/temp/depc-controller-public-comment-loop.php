<?php

/**
 * @link       http://webnus.biz
 * @since      1.0.0
 *
 * @package    Depper Comment
 */

class Depc_Controller_Public_Comment_Loop extends Depc_Controller_Public_Comment {

	private static $comment_loop;
	private $settings;

	/**
	 * Constructor
	 *
	 * @since    1.0.0
	 */
	protected function __construct() {
		// get instance from comment loop model
		$this->model = Depc_Model_Public_Comment_Loop::get_instance();
		// get delete options
		$logged_in = is_user_logged_in();
		$this->settings['delete'] 		= ( $logged_in == true ) ? Depc_Core::get_option( 'dc_delete_member', 'Comment_Settings' ) : Depc_Core::get_option( 'dc_delete_guest', 'Comment_Settings' ) ;

		// get edite options
		$this->settings['edit']  		= ( $logged_in == true ) ? Depc_Core::get_option( 'dc_edit_member', 'Comment_Settings' ) :  Depc_Core::get_option( 'dc_edit_guest', 'Comment_Settings' );

		// get user flag options
		$this->settings['inappropriate_member'] = Depc_Core::get_option( 'dc_inappropriate_members', 'Inappropriate_Comments' );
		$this->settings['inappropriate_guest']  = Depc_Core::get_option( 'dc_inappropriate_guest', 'Inappropriate_Comments' );
		
		// get link options
		$this->settings['link']  		=  ( $logged_in == true ) ? Depc_Core::get_option( 'dc_link_member', 'Comment_Settings' ) : Depc_Core::get_option( 'dc_link_guest', 'Comment_Settings' );

		// get collapse options
		$this->settings['collapse']  	= ( $logged_in == true ) ? Depc_Core::get_option( 'dc_collapse_member', 'Comment_Settings' ) : Depc_Core::get_option( 'dc_collapse_guest', 'Comment_Settings' );

		//get vote options
		$this->settings['vote_enable'] 	= ( $logged_in == true ) ? Depc_Core::get_option( 'dc_vote_user_enable', 'Vote_Options' ) : Depc_Core::get_option( 'dc_vote_guest_enable', 'Vote_Options' ) ;


		// get collapse options
		$this->settings['social_enable']   		= Depc_Core::get_option( 'dc_social_enable', 'Social_Share' );
		$this->settings['social_share_fb']   	= Depc_Core::get_option( 'dc_social_share_fb', 'Social_Share' );
		$this->settings['social_share_tw']   	= Depc_Core::get_option( 'dc_social_share_tw', 'Social_Share' );
		$this->settings['social_share_mail']   	= Depc_Core::get_option( 'dc_social_share_mail', 'Social_Share' );

 	}

 	public static function get_comment_loop() {

 		if ( null === self::$comment_loop ) {
 			self::$comment_loop = new self();
 		}
 		return self::$comment_loop;

	}

 	private function restriction() {

 		// check if show for members
 		if ( is_user_logged_in() ) {

 			if ( $this->settings['inappropriate_member'] == 'on' ){
 				$render = 					
 				'
 				<span> | </span>
 				<a href="#" class="dpr-discu-flag dpr-tooltip" data-wntooltip=" '. esc_attr__( "Flag as inappropriate", "depc" ).' ">
 					<i class="sl-flag"></i>
 				</a>';
 				return $render;
 			} else {
 				return false;
 			}

 		} elseif ( !is_user_logged_in() ) {

 			if ( $this->settings['inappropriate_guest'] == 'on' ) {
 				$render = 					
	 				'
	 				<span> | </span>
	 				<a href="#" class="dpr-discu-flag dpr-tooltip" data-wntooltip=" '. esc_attr__( "Flag as inappropriate", "depc" ).' ">
		 				<i class="sl-flag"></i>
		 			</a>';
	 			return $render;
 			} else{
 				return false;
 			}
 		}

 	}

 	public function get_loadmore() {
 		$loadmore = Depc_Controller_Public_Comment_Loadmore::get_instance();
 		return $loadmore;
 	}	

	
	public function load( $run = false , $post_id = null ) {

		// get current post id 
		$id = ( new self )->post_id();
		$comment = static::get_model()->get_comment();
		$comment_parent = static::get_model()->get_comment_parent();
		$validator = static::get_model()->get_validator();

		$nonce = wp_create_nonce( 'dpr-social' );
		if ( $run == true ) {
			$id = $post_id;
			$comment = $run;
			return static::render_template(
				'tpl/loop.php',
				array(
					'id'       		    => $id,
					'comments'          => $comment,
					'settings'  	    => $this->settings,
					'inappropriate'     => $this->restriction(),
					'comments_parrents' => $comment_parent,
					'validator' 		=> $validator,
					'nonce'  			=> $nonce,
					'load_more'  		=> $this->get_loadmore()
				),
				'always'
			);
		}

		echo static::render_template(
			'tpl/loop.php',
			array(
				'id'       		    => $id,
				'comments'          => $comment,
				'settings'  	    => $this->settings,
				'inappropriate'     => $this->restriction(),
				'comments_parrents' => $comment_parent,
				'validator' 		=> $validator,
				'nonce'  			=> $nonce,
				'load_more'  		=> $this->get_loadmore()
				),
			'always'
		);

	}

}