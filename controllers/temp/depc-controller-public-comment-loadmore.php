<?php

/**
 * @link       http://webnus.biz
 * @since      1.0.0
 *
 * @package    Depper Comment
 */

class Depc_Controller_Public_Comment_Loadmore extends Depc_Controller_Public {
	/**
	 * Constructor
	 *
	 * @since    1.0.0
	 */
	protected function __construct() {

		$this->validator = new Depc_Request_Validator;
		$this->model = Depc_Model_Public_Comment_Loadmore::get_instance();
		$this->settings = $this->get_user_settings();

	}

	public function scripts() {
		$script = static::get_model()->scripts();
		return $script;
	}	

	public function load( $id ) {


		if ( $id == 0 ) { 
			$url = wp_get_referer();
			$id = url_to_postid( $url ); 
		}

		if ( $this->settings['loadmore']  == 'off' ) return;

		// get current post id 
		$id = (int) static::get_model()->parent_comments( $id ) - (int) $this->settings['defultcnt_comment'];
		$this->settings['default_sorting'] = $this->validator->get_comment_order( $this->settings['default_sorting'] );

		if ( $this->settings['defultcnt_comment'] >= $id  ) return;

		$out = '
		<div class="dpr-loadmore-wrap">
			<a href="#" class="dpr-loadmore-btn" data-comments="'.$id.'" data-type="'.$this->settings['default_sorting'].'" data-loaded="'.$this->settings['defultcnt_comment'].'" >'.esc_attr__( 'Load More', 'depc' ).'</a>
		</div>
		<div class="dpr-preloadmore-wrap">
			<div class="dpr-preloadmore"></div>
		</div>
		';

		return $out;
	}

}