<?php

/**
 * @link       http://webnus.biz
 * @since      1.0.0
 *
 * @package    Depper Comment
 */

class Depc_Loader {

	/**
	 * DEPC Instance
	 */
	private static $instance;

	/**
	 * Provides access to a single instance of a module using the singleton pattern
	 */
	public static function get_instance() {

		if ( null === self::$instance ) {
			self::$instance = new self();
		}
		return self::$instance;

	}

	/**
	 * Constructor
	 *
	 * @since    1.0.0
	 */
	protected function __construct() {

		spl_autoload_register( array( $this, 'depc_load' ) );

		$this->set_locale();
		$this->register_hook_callbacks();

	}

	/**
	 * Loads all Plugin dependencies
	 *
	 * @since    1.0.0
	 */
	private function depc_load( $class ) {

		if ( false !== strpos( $class, Depc_Core::CLASS_PREFIX ) ) {

			$cname = str_replace( '_', '-', strtolower( $class ) ) . '.php';
			$folder        = '/';

			if ( false !== strpos( $class, '_Admin' ) ) {
				$folder .= 'admin/';
			}			
			if ( false !== strpos( $class, '_Comment' ) ) {
				$folder .= 'temp/';
			}			

			if ( false !== strpos( $class, '_Module' ) ) {
				$folder .= 'module/';
			}

			if ( false !== strpos( $class, Depc_Core::CLASS_PREFIX . 'Controller' ) ) {
				$path = Depc_Core::get_depc_path() . 'controllers' . $folder . $cname;
				require_once( $path );
			} elseif ( false !== strpos( $class, Depc_Core::CLASS_PREFIX . 'Model' ) ) {
				$path = Depc_Core::get_depc_path() . 'models' . $folder . $cname;
				require_once( $path );
			} else {
				$path = Depc_Core::get_depc_path() . 'lib/' . $cname;
				require_once( $path );
			}

		}

	}

	/**
	 * Define the locale for depc for internationalization.
	 *
	 * @since    1.0.0
	 */
	private function set_locale() {

		$plugin_i18n = new Depc_i18n();
		$plugin_i18n->set_domain( 'depc' );

		Depc_Actions_Filters::add_action( 'plugins_loaded', $plugin_i18n, 'textdomain' );

	}

	/**
	 * Register callbacks for actions and filters
	 *
	 * @since    1.0.0
	 */
	public function register_hook_callbacks() {

		register_activation_hook(   Depc_Core::get_depc_path() . 'deeper-comments.php', array( $this, 'activate' ) );
		register_deactivation_hook( Depc_Core::get_depc_path() . 'deeper-comments.php', array( $this, 'deactivate' ) );

		Depc_Controller_Admin_Inapp::get_instance();

	}	

	/**
	 * Prepares sites to use the plugin during single or network-wide activation
	 *
	 * @since    1.0.0
	 * @param bool $network_wide
	 */
	public function activate( $network_wide ) {

		$options = $this->get_default_options();

		foreach ( $options as $option => $optionfield ) {
			if ( get_option( $option ) == false ) {
				update_option(  $option , $optionfield );
			}
		}

	}

	private function get_default_options() {
		
		$options = array(
			'Comment_Filter_Bar' => 'a:3:{s:23:"dc_enable_filter_member";s:2:"on";s:22:"dc_enable_filter_guest";s:2:"on";s:18:"dc_default_sorting";s:6:"oldest";}',
			'Recaptcha' => 'a:6:{s:17:"dc_recaptcha_type";s:4:"none";s:20:"dc_recptcha_gsitekey";s:0:"";s:22:"dc_recptcha_gsecretkey";s:0:"";s:18:"dc_recaptcha_addcm";s:3:"off";s:18:"dc_recaptcha_theme";s:5:"light";s:17:"dc_recaptcha_size";s:6:"normal";}',
			'Avatar' => 'a:1:{s:18:"dc_generate_avatar";s:2:"on";}',
			'Comment_Settings' => 'a:11:{s:20:"dc_defultcnt_comment";s:1:"7";s:16:"dc_delete_member";s:3:"off";s:15:"dc_delete_guest";s:3:"off";s:14:"dc_edit_member";s:2:"on";s:13:"dc_edit_guest";s:3:"off";s:18:"dc_edit_expiration";s:1:"1";s:14:"dc_link_member";s:2:"on";s:13:"dc_link_guest";s:2:"on";s:18:"dc_collapse_member";s:2:"on";s:17:"dc_collapse_guest";s:2:"on";s:22:"dc_allow_guest_comment";s:3:"off";}',
			'Login_Register' => 'a:2:{s:14:"dc_quick_login";s:2:"on";s:17:"dc_quick_register";s:2:"on";}',
			'Google' => 'a:4:{s:22:"dc_google_login_enable";s:3:"off";s:25:"dc_social_login_g_appname";s:0:"";s:24:"dc_social_login_g_client";s:0:"";s:24:"dc_social_login_g_secret";s:0:"";}',
			'Vote_Options' => 'a:4:{s:19:"dc_vote_user_enable";s:2:"on";s:17:"dc_vote_user_date";s:2:"10";s:20:"dc_vote_guest_enable";s:2:"on";s:18:"dc_vote_guest_date";s:2:"10";}',
			'Social_Share' => 'a:4:{s:16:"dc_social_enable";s:2:"on";s:18:"dc_social_share_fb";s:2:"on";s:18:"dc_social_share_tw";s:2:"on";s:20:"dc_social_share_mail";s:2:"on";}',
			'Inappropriate_Comments' => 'a:3:{s:24:"dc_inappropriate_members";s:2:"on";s:22:"dc_inappropriate_guest";s:2:"on";s:17:"dc_inapp_auto_ban";s:1:"5";}'
		);

		return $options;
	}

	/**
	 * Rolls back activation procedures when de-activating the plugin
	 *
	 * @since    1.0.0
	 */
	public function deactivate() {

		Depc_Model_Admin_Notices::remove_admin_notices();

	}

	/**
	 * Fired when user uninstalls the plugin, called in unisntall.php file
	 *
	 * @since    1.0.0
	 */
	public static function uninstall_plugin() {

		require_once dirname( plugin_dir_path( __FILE__ ) ) . '/lib/depc-core.php';
		require_once dirname( plugin_dir_path( __FILE__ ) ) . '/models/depc-model.php';
		require_once dirname( plugin_dir_path( __FILE__ ) ) . '/models/admin/depc-model-admin.php';
		require_once dirname( plugin_dir_path( __FILE__ ) ) . '/models/admin/depc-model-admin-settings.php';
		
		Depc_Model_Admin_Settings::delete_settings();

	}

}
