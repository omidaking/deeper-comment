<?php


class Depc_Settings {

	private $settings;

	public function __construct() {

		$this->register_hook_callbacks();

	}

	public function register_hook_callbacks() {

		Depc_Actions_Filters::add_action( 'plugins_loaded', $this, 'get_user_option' );

	}

	public function get_user_option() {

		$this->settings['memberfilter'] = Depc_Core::get_option( 'dc_enable_filter_member', 'Comment_Filter_Bar' );
		$this->settings['guestfilter']  = Depc_Core::get_option( 'dc_enable_filter_guest', 'Comment_Filter_Bar' );
		$this->settings['default_sorting']  = Depc_Core::get_option( 'dc_default_sorting', 'Comment_Filter_Bar' );
		$this->settings['generate_avatar']  = Depc_Core::get_option( 'dc_generate_avatar', 'Avatar' );
		$this->settings['defultcnt_comment']  = Depc_Core::get_option( 'dc_defultcnt_comment', 'Comment_Settings' );
		$this->settings['loadmore']  = Depc_Core::get_option( 'dc_enable_loadmore', 'Load_More' );
		$this->settings['loadmore_count']  = Depc_Core::get_option( 'dc_enable_loadmore_count', 'Load_More' );

		return $this->settings; 

	}

}