/**
 * Created by Webnus01 on 6/24/2017.
 */
(function($)
{
    /**
     *  Start Submit Comment
     */
     $.fn.depcSubmitComment = function(options)
     {
		// Default Options
		var settings = $.extend({
				// These are the defaults.
				id: 0,
				editor : 0,
				nounce : '',
				reply : true,
				selector : '',
				logged : true,
				captcha : null

			}, options);

		// Init Sliders
		if ( settings.reply === false ) {
			initAddComment(); 
		} else if ( settings.reply === true ) {
			initAddReplyComment();
		} else if( settings.reply === 1 ) {
			commentReplySaveButton();
		}


		function initAddComment() {
			$(".dpr-discu-container_"+settings.id+" "+settings.selector).on('click', function(){

				var $this = $(this) , is_hnext = '', is_hsubmit = '' , recaptcha = '' , gemail , gname , getcaptcha;
				var html_data;
				var human_data = sessionStorage.getItem( 'dpr_text' );
				// get comment content
				if ( typeof( human_data ) === "undefined" || human_data === null ) { human_data = ''; }
				// check if user is logged in
				if ( settings.logged === '0' ) { is_hnext = false; is_hsubmit = true; }else{ is_hnext = true; is_hsubmit = false;} 

				var
				submitfrm = 
				$.confirm({
					content: "<script>tinymce.init({selector: '.dpr-add-editor',height: 260,branding: false,statusbar: 'large',theme: 'modern',menubar: false});</script><textarea class='dpr-add-editor'>" +  human_data + "</textarea>",
					title: dpr.editor_title,
					type: 'blue',
					icon: 'fa fa-edit',
					rtl: dpr.rtl,
					animation: 'bottom',
					closeAnimation: 'top',
					columnClass: 'large',
					theme: 'supervan',
					buttons: {
						next: {
						    isHidden: is_hnext,
							action: function(){
								html_data  = tinyMCE.activeEditor.getContent();
								$this.val( $('<div/>').html(html_data).text() );
								sessionStorage.setItem( 'dpr_text', html_data );
								tinymce.remove('.dpr-add-editor');

								if ( settings.captcha ===  'on' && settings.logged === '0'  ) recaptcha = '<div id="dpr-submit-captcha"></div>' ; 

								$('.dpr-add-editor').css('display', 'none');
								submitfrm.setContentPrepend( '<div class="dpr-modal-cntt dpr-modal-midsize"><input type="text" name="dpr-gname" id="dpr-gname" placeholder="'+dpr.name+'"><input type="text" name="dpr-gemail" id="dpr-gemail" placeholder="'+dpr.email+'">'+recaptcha+'</div>' );

								// check for settings
								if ( settings.captcha ===  'on' && settings.logged === '0' ) {
									grecaptcha.render( 'dpr-submit-captcha', {
										'sitekey' : dpr.recaptcha, 
										'theme' : dpr.recaptcha_theme,
										'size' : dpr.recaptcha_size
									});
								}

								this.buttons.ok.show();
								this.buttons.next.hide();
								return false;
							}
						},
						ok: {
						    text: dpr.post_comment,
						    btnClass: 'btn-primary',
						    isHidden: is_hsubmit, 
						    keys: ['enter'],
						    action: function(){
						    	if ( is_hnext === true ) {
						    		html_data  = tinyMCE.activeEditor.getContent();
						    		$this.val( $('<div/>').html(html_data).text() );
						    		sessionStorage.setItem( 'dpr_text', html_data );
						    	}
						        $('.dpr-preloader-wrap').css('display', 'block');
								gemail = $('#dpr-gemail').val();
								gname = $('#dpr-gname').val();
								getcaptcha = $('#dpr-submit-captcha .g-recaptcha-response').val();

						        var
						        jqXhr =
						        $.ajax( {
						            type: "POST",
						            url: dpr.adminajax,
						            data: {
						            	action : 'dpr_add_comment',
						            	security : settings.nounce,
						            	post_id : settings.id,
						            	comment_data : html_data,
						            	gemail : gemail,
						            	gname  : gname,
						            	recaptcha  : getcaptcha
						            },
						        });

								$.when(jqXhr).done(function(data) {
									// display block
									$('.dpr-preloader-wrap').css('display', 'none');

									// if publish
									if ( data.status === 'publish' ) {
										setTimeout(function() {
											$('.dpr-preloader-wrap').css('display', 'none');
											$( data.data ).insertAfter( ".dpr-discu-container_"+settings.id+ " .dpr-join-form-wrap" );
										}, 400);						                	
									} else {
										$.alert({
											text: dpr.ok,
											title: dpr.duplicate,
											content: data.data,
										});
									}

								})

		                    }
		                },
		                cancel: {
		                    text: dpr.cancle,
		                    action: function(){
		                        html_data  = tinyMCE.activeEditor.getContent();
		                        $this.val( $('<div/>').html(html_data).text() );
		                        sessionStorage.setItem( 'dpr_text', html_data );
		                    }
		                }
		            }
		        });

		    });
		}

		function initAddReplyComment() {
			var selector_spn = '' , selector_btn = '';
			$( ".dpr-discu-container_"+settings.id+" "+settings.selector ).toggle(function(e) {
			   e.preventDefault();
			   // define some vars
			   var $this = $(this) , comment_id = $this.data('id') , parent_id = $this.data('parent') , recaptcha = '';

			   if ( comment_id === parent_id ) {
			   	selector_btn = '.dpr-tinymce-button:first'; 
			   	selector_spn = '.dpr-tinymce-replies:first'; 
			   } else {
			   	selector_spn = '.dpr-tinymce-replies'; 
			   	selector_btn = '.dpr-tinymce-button';
			   }

				// init tinymce
				dpr_tinymce(".dpr-discu-wrap_"+comment_id+" .dpr-discu-replies-wrap "+selector_spn, 100 , false , 'small');
				if ( settings.captcha ===  'on' && settings.logged === '0'  ) recaptcha = '<div class="dpr-replies-captcha" id="dpr-captcha-'+comment_id+'"></div>' ; 
				// append button
				$( ".dpr-discu-wrap_"+comment_id+" .dpr-discu-replies-wrap "+selector_btn ).append( recaptcha+'<a href="#" data-id="'+comment_id+'" data-parent="'+parent_id+'" class="dpr_add_reply_comment">' + dpr.save_comment + '</a> <a href="#" data-id="'+comment_id+'" data-parent="'+parent_id+'" class="dpr_cancle_reply_comment">' + dpr.cancle + '</a>');
				
				// check for settings
				if ( settings.captcha ===  'on' && settings.logged === '0' ) {
					grecaptcha.render( 'dpr-captcha-'+comment_id, {
						'sitekey' : dpr.recaptcha, 
						'theme' : dpr.recaptcha_theme,
						'size' : dpr.recaptcha_size
					});
				}

			}, function() {
				var $this = $(this) , comment_id = $this.data('id');
				// remove cancle and edit comment
				$( ".dpr-discu-wrap_"+comment_id+" .dpr-discu-replies-wrap "+selector_btn+" .dpr_add_reply_comment , .dpr-discu-wrap_"+comment_id+" .dpr-discu-replies-wrap "+selector_btn+" .dpr_cancle_reply_comment , .dpr-discu-wrap_"+comment_id+" .dpr-discu-replies-wrap "+selector_btn+" #dpr-captcha-"+comment_id ).remove();
			    tinymce.remove(".dpr-discu-wrap_"+comment_id+" .dpr-discu-replies-wrap "+selector_spn);
			});
		}

		function commentReplySaveButton() {

        	$(document).on('click', ".dpr-discu-container_"+settings.id+" "+settings.selector , function(event) {
        		event.preventDefault();
        		// define some vars
        		var comment_id = $(this).data('id');
        		var parent_id  = $(this).data('parent');
        		// get edited data from tiny mce
        		html_data  = tinyMCE.activeEditor.getContent();
        		recaptcha  = $( '#dpr-captcha-'+comment_id+' .g-recaptcha-response' ).val();

				// loading Class
				$('.dpr-preloader-wrap').css('display', 'block');

				$.ajax( {
					type: "POST",
					url: dpr.adminajax,
					data: {
						action : 'dpr_add_comment',
						security : settings.nounce,
						comment_id : comment_id,
						post_id : settings.id,
						comment_data : html_data,
						recaptcha : recaptcha
					},
					success: function(data) {
						// Remove the loading Class
						$('.dpr-preloader-wrap').css('display', 'none');
						if ( data.status === 'publish' ) {
							setTimeout(function() {
								$( data.data ).insertAfter( ".dpr-discu-wrap_"+parent_id+" .dpr-discu-box-footer .dpr-discu-replies-wrap:first" );
							}, 400);
						} else {
							$.alert({
								text: dpr.ok,
								title: dpr.duplicate,
								content: data.data,
							});
						}
						// remove tiny mce
						if ( comment_id === parent_id ) $( ".dpr-discu-wrap_"+comment_id+" .dpr-discu-box-footer .dpr-discu-reply-btn-wrap .dpr-discu-reply-btn" ).first().trigger('click'); else  $( ".dpr-discu-wrap_"+comment_id+" .dpr-discu-box-footer .dpr-discu-reply-btn-wrap .dpr-discu-reply-btn" ).trigger('click');
						$(".dpr-discu-wrap_"+comment_id+" .dpr-discu-replies-wrap .dpr-tinymce-replies p").html('');
						
						setTimeout(function() {							
							var target = $("#comments-"+data.newcomment_id);
							target = target.length ? target : $('[name=' + this.hash.substr(1) +']');
							if (target.length) {
								$('html,body').animate({
									scrollTop: target.offset().top
								}, 800);
								return false;
							}
						}, 401);

					},
					error: function(jqXHR, textStatus, errorThrown) {
		                // Remove the loading Class
		                setTimeout(function() {
		                	$('.dpr-preloader-wrap').css('display', 'none');
		                }, 100);
		            }
		        });

			});
        }

    };
    /**
     *  End Submit Comment
     */

     /**
     *  Start Edit Comment
     */
     $.fn.depcEditComment = function(options)
     {
        // Default Options
        var settings = $.extend(
        {
                // These are the defaults.
                id: 0,
                save : true,
                nounce : ''
            }, options);

		if ( settings.save == false ) {
		    commentEditButton();
		} else if ( settings.save === true ) {
		   commentEditSaveButton();
		} else if ( settings.save === null ) {
			commentDeleteButton();
		} else if ( settings.save === -1 ) {
			commentFlagButton();
		} else if ( settings.save === -2 ) {
			commentSocialButton();
		}

		function commentEditButton() {

			$(document).on('click', ".dpr-discu-container_"+settings.id+" .dpr-discu-wrap .dpr-discu-box-header-icons .dpr-discu-edit" , function(event) {
				event.preventDefault();
				toggledit();
				var $this = $(this);
				if ( $( ".dpr-discu-wrap_"+$this.data('id')+" .dpr-discu-text:first p[id]" ) != 'mce_0' ) { $this.trigger('click'); }
			});

		}

		function toggledit() {
			$(".dpr-discu-container_"+settings.id+" .dpr-discu-wrap .dpr-discu-box-header-icons .dpr-discu-edit" ).toggle(function() {
				var $this = $(this);
				var comment_id = $this.data('id');
				dpr_tinymce(".dpr-discu-wrap_"+comment_id+" .dpr-discu-text:first p" , 100 , false , 'small');
				$( ".dpr-discu-wrap_"+comment_id+" .dpr-discu-text:first" ).append('<a href="#" data-id="'+comment_id+'" class="dpr_edit_comment">' + dpr.save_comment + '</a> <a href="#" data-id="'+comment_id+'" data-parent="'+comment_id+'" class="dpr_cancle_comment">' + dpr.cancle + '</a>');

			}, function() {
				var $this = $(this);
				var comment_id = $this.data('id');
				$( ".dpr-discu-wrap_"+comment_id+" .dpr-discu-text:first .dpr_edit_comment , .dpr-discu-wrap_"+comment_id+" .dpr-discu-text:first .dpr_cancle_comment" ).remove();
				tinymce.remove(".dpr-discu-wrap_"+comment_id+" .dpr-discu-text:first p");
			});

        }

        function commentEditSaveButton() {

        	$(document).on('click', '.dpr-discu-container_'+settings.id+' .dpr-discu-wrap .dpr_edit_comment' , function(event) {
        		event.preventDefault();
        		var $this = $(this);
        		var comment_id = $this.data('id');
        		html_data  = tinyMCE.activeEditor.getContent();

				// loading Class
				$('.dpr-preloader-wrap').css('display', 'block');

				$.ajax( {
					type: "POST",
					url: dpr.adminajax,
					data: {
						action : 'dpr_edit_comment',
						security : settings.nounce,
						comment_id : comment_id,
						content : html_data
					},
					success: function(data) {
						// Remove the loading Class
						$('.dpr-preloader-wrap').css('display', 'none');
						$( ".dpr-discu-wrap_"+comment_id+" .dpr-discu-text .dpr_edit_comment , .dpr-discu-wrap_"+comment_id+" .dpr-discu-text .dpr_cancle_comment" ).remove();
						tinymce.remove(".dpr-discu-wrap_"+comment_id+" .dpr-discu-text p");
						$(".dpr-discu-wrap_"+comment_id+" .dpr-discu-text").empty();
						$(".dpr-discu-wrap_"+comment_id+" .dpr-discu-text").append(html_data);

					},
					error: function(jqXHR, textStatus, errorThrown) {
		                // Remove the loading Class
		                setTimeout(function() {
		                	$('.dpr-preloader-wrap').css('display', 'none');
		                }, 100);
		            }
		        });

			});
        }

        function commentDeleteButton() {

        	$(document).on('click', '.dpr-discu-container_'+settings.id+' .dpr-discu-wrap .dpr-discu-delete' , function(event) {
        		event.preventDefault();
        		var $this = $(this);
        		var comment_id = $this.data('id');

                $.confirm({
                    content: dpr.sure_delete,
                    title: dpr.delete_cm,
                    type: 'red',
                    rtl: dpr.rtl,
                    animation: 'bottom',
                    closeAnimation: 'top',
                    theme: 'modern',
                    buttons: {
                        ok: {
                            text: dpr.delete,
                            btnClass: 'btn-primary',
                            keys: ['enter'],
                            action: function(){
            	        		// loading Class
            	        		$('.dpr-preloader-wrap').css('display', 'block');

                                $.ajax( {
                                	type: "POST",
                                	url: dpr.adminajax,
                                	data: {
                                		action : 'dpr_delete_comment',
                                		security : settings.nounce,
                                		comment_id : comment_id
                                	},
                                	success: function(data) {
				        				// Remove the loading Class
				        				$('.dpr-preloader-wrap').css('display', 'none');
				        				$('.dpr-discu-wrap_'+comment_id).fadeOut(800);

				        			},
				        			error: function(jqXHR, textStatus, errorThrown) {
				                        // Remove the loading Class
				                        setTimeout(function() {
				                        	$('.dpr-preloader-wrap').css('display', 'none');

				                        }, 100);
				                    }
				                }); 
                            }
                        },
                        cancel: {
                            text: dpr.cancle,
                            action: function(){
                            }
                        }
                    }
                });

            });
        }

        function commentFlagButton() {

        	$(document).on('click', '.dpr-discu-container_'+settings.id+' .dpr-discu-wrap .dpr-discu-flag' , function(event) {
        		event.preventDefault();

        		var $this = $(this);
        		var comment_id = $this.data('id');

        		$.confirm({
        			content: dpr.sure_flag,
        			title: dpr.flag_cm,
        			type: 'red',
        			rtl: dpr.rtl,
        			animation: 'bottom',
        			closeAnimation: 'top',
        			theme: 'modern',
        			buttons: {
        				ok: {
        					text: dpr.ok,
        					btnClass: 'btn-primary',
        					keys: ['enter'],
        					action: function(){
            	        		// loading Class
            	        		$('.dpr-preloader-wrap').css('display', 'block');
            	        		$.ajax( {
            	        			type: "POST",
            	        			url: dpr.adminajax,
            	        			data: {
            	        				action : 'dpr_flag_comment',
            	        				security : settings.nounce,
            	        				comment_id : comment_id
            	        			},
            	        			success: function(data) {
				        				// Remove the loading Class
				        				$('.dpr-preloader-wrap').css('display', 'none');
				        				$.alert({
				        					text: dpr.ok,
				        					title: dpr.flag_cm,
				        					content: data.message,
				        				});
				        			},
				        			error: function(jqXHR, textStatus, errorThrown) {
				                        // Remove the loading Class
				                        setTimeout(function() {
				                        	$('.dpr-preloader-wrap').css('display', 'none');

				                        }, 100);
				                    }
				                }); 
            	        	}
            	        },
            	        cancel: {
            	        	text: dpr.cancle,
            	        	action: function(){

            	        	}
            	        }
            	    }
            	});

        	});

        }

		function commentSocialButton() {

			$(document).on('click', '.dpr-discu-container_'+settings.id+' .dpr-discu-metadata-share-wrap .dpr-discu-sharing .dpr-discu-social-icon a' , function(event) {
				event.preventDefault();
				var $this = $(this) , comment_id = $this.data('id');

				var child = parseInt( $( '.dpr-discu-wrap .dpr-c-contents .dpr-discu-metadata-share-wrap .dpr-discu-share .dpr-discu-share-count-'+comment_id ).html() );
				$( '.dpr-discu-wrap .dpr-c-contents .dpr-discu-metadata-share-wrap .dpr-discu-share .dpr-discu-share-count-'+comment_id ).text( child + 1 );

				$.ajax( {
					type: "POST",
					url: dpr.adminajax,
					data: {
						action : 'dpr_social_comment',
						security : settings.nounce,
						comment_id : comment_id
					},
					success: function(data) {
						if ( data.resp == 0 ) { $( '.dpr-discu-wrap .dpr-discu-metadata-share-wrap .dpr-discu-share .dpr-discu-share-count-'+comment_id ).text( child - 0 ); }
					},
					error: function(jqXHR, textStatus, errorThrown) {
							
				    }
				}); 

			});

        }

    };
    /**
     *  End Edit Comment
     */

    /**
     *  Start Login Form
     */
     $.fn.depcLoginForm = function(options)
     {
        // Default Options
        var settings = $.extend(
        {
                // These are the defaults.
                id: 0,
                nounce : '',
                register: ''
            }, options);

        // Init Sliders
        if ( settings.register == false ) initLoginForm(); else initRegisterForm();

        function initLoginForm() {

            $(".dpr-discu-container_"+settings.id+" .dpr-join-form-login-a").on('click', function(){
                var $this = $(this);
            var sing = 
                $.confirm({
                    content:"<div class='dpr-modal-cntt'><form id='login' action='login' method='post'><input placeholder='"+dpr.username+"' id='dpr_username' type='text' name='username'><input placeholder='"+dpr.password+"' id='dpr_password' type='password' name='password'></form></div>",
                    title: dpr.login,
                    type: 'red',
                    rtl: dpr.rtl,
                    animation: 'bottom',
                    closeAnimation: 'top',
                    theme: 'modern',
                    onOpen: function(){

                    },
                    buttons: {
                        ok: {
                            text: dpr.login,
                            btnClass: 'btn-primary',
                            keys: ['enter'],
                            action: function(){
                                $('.dpr-preloader-wrap').css('display', 'block');

                                var username , password ;
                                username = $('#dpr_username').val();
                                password = $('#dpr_password').val();

                                $.ajax( {
                                    type: "POST",
                                    url: dpr.adminajax,
                                    data: {
                                        action : 'dpr_login',
                                        security : settings.nounce,
                                        post_id : settings.id,
                                        dpr_username : username,
                                        dpr_password : password

                                    },
                                    success: function(data) {
                                        $('.dpr-preloader-wrap').css('display', 'none');
                                        if ( data.error == true ) {
                                            $('.dpr-notify').remove();
                                            sing.setContentAppend('<div class="dpr-notify">'+data.message+'</div>');
                                        } else {
                                            $('.dpr-notify').remove();
                                            sing.setContentAppend('<div class="dpr-notify">'+data.message+'</div>');
                                            setTimeout(function() {
                                                location.reload();
                                            }, 1000);
                                        }
                                        // Remove the loading Class to the button
                                        $('.dpr-preloader-wrap').css('display', 'none');

                                    },
                                    error: function(jqXHR, textStatus, errorThrown) {
                                        // Remove the loading Class to the button
                                        $('.dpr-preloader-wrap').css('display', 'none');
                                    }
                                });
                                return false;

                            }
                        },
                        cancel: {
                            text: dpr.cancle,
                            action: function(){

                            }
                        }
                    }
                });
            });
        }

        function initRegisterForm() {
            $(".dpr-discu-container_"+settings.id+" .dpr-join-form-register-a").on('click', function(){
                var $this = $(this);

            var sing =
                $.confirm({
                    content: "<div class='dpr-modal-cntt'><form id='dpr-register' action='login' method='post'><input type='text' name='name' placeholder="+dpr.name+"><input type='email' name='email' placeholder="+dpr.email+"><input type='text' name='username' placeholder="+dpr.username+"><input id='inputPassword' type='password' name='password' placeholder="+dpr.password+"><input type='password' name='cnfrm_password' placeholder="+dpr.confirm_password+"><span id='complexity' class='default'>Enter a random value</span><label class='checkbox rememberme'><input name='rememberme' type='checkbox' id='rememberme' value='forever'><span>I have read and accept the <a href='#' target='_blank' class='dpr-modal-terms'>Terms and Conditions</a></span></label></div><div class='dpr-modal-footer'><span class='dpr-modal-login-span'>Already have an account? <a href='#' class='dpr-modal-login-a'>"+dpr.login+"</a></span></div></form></div>",
                    title: dpr.singup,
                    type: 'red',
                    rtl: dpr.rtl,
                    animation: 'bottom',
                    closeAnimation: 'top',
                    theme: 'modern',
                    onOpen: function(){
                        jQuery("#inputPassword").depcPassStrength();
                    },
                    buttons: {
                        ok: {
                            text: dpr.singup,
                            btnClass: 'btn-primary',
                            keys: ['enter'],
                            action: function(){
                                $('.dpr-preloader-wrap').css('display', 'block');

                                var register = $('#dpr-register').serialize();

                                $.ajax( {
                                    type: "POST",
                                    url: dpr.adminajax,
                                    data: {
                                        action   : 'dpr_register',
                                        security : settings.nounce,
                                        post_id  : settings.id,
                                        register : register
                                    },
                                }).done( function(data) {
                                    $('.dpr-preloader-wrap').css('display', 'none');
                                    if ( data.error == true ) {
                                        $('.dpr-notify').remove();
                                        $.each(data.message,function(index, el) {
                                            sing.setContentAppend('<div class="dpr-notify">'+el+'</div>');
                                        });
                                    } else {
                                        $('.dpr-notify').remove();
                                        $.each(data.message,function(index, el) {
                                            sing.setContentAppend('<div class="dpr-notify">'+el+'</div>');
                                        });
                                        setTimeout(function() {
                                            location.reload();
                                        }, 1000);
                                    }
                                }).fail(function(){
                                    $('.dpr-preloader-wrap').css('display', 'none');
                                });

                                return false;
                            }
                        },
                        cancel: {
                            text: dpr.cancle,
                            action: function(){

                            }
                        }
                    }
                });
            });
        }
    }; 
     /**
     *  End Login Form
     */    

	/**
	 *  Start Password strength
	 */
	$.fn.depcPassStrength = function() {
		// Default Options
		var strPassword;
		var charPassword;
		var complexity = $("#complexity");
		var minPasswordLength = 8;
		var baseScore = 0, score = 0;
		
		var num = {};
		num.Excess = 0;
		num.Upper = 0;
		num.Numbers = 0;
		num.Symbols = 0;

		var bonus = {};
		bonus.Excess = 3;
		bonus.Upper = 4;
		bonus.Numbers = 5;
		bonus.Symbols = 5;
		bonus.Combo = 0; 
		bonus.FlatLower = 0;
		bonus.FlatNumber = 0;
		
		outputResult();
		$("#inputPassword").bind("keyup", checkVal);

		function checkVal() {
			init();
			
			if (charPassword.length >= minPasswordLength) {
				baseScore = 50;	
				analyzeString();	
				calcComplexity();		
			} else {
				baseScore = 0;
			}
			
			outputResult();
		}

		function init() {

			strPassword= $("#inputPassword").val();
			charPassword = strPassword.split("");
				
			num.Excess = 0;
			num.Upper = 0;
			num.Numbers = 0;
			num.Symbols = 0;
			bonus.Combo = 0; 
			bonus.FlatLower = 0;
			bonus.FlatNumber = 0;
			baseScore = 0;
			score =0;
		}

		function analyzeString () {	
			for (i=0; i<charPassword.length;i++) {
				if (charPassword[i].match(/[A-Z]/g)) {num.Upper++;}
				if (charPassword[i].match(/[0-9]/g)) {num.Numbers++;}
				if (charPassword[i].match(/(.*[!,@,#,$,%,^,&,*,?,_,~])/)) {num.Symbols++;} 
			}
			
			num.Excess = charPassword.length - minPasswordLength;
			
			if (num.Upper && num.Numbers && num.Symbols) {
				bonus.Combo = 25; 
			} else if ((num.Upper && num.Numbers) || (num.Upper && num.Symbols) || (num.Numbers && num.Symbols)){
				bonus.Combo = 15; 
			}
			
			if (strPassword.match(/^[\sa-z]+$/)){ 
				bonus.FlatLower = -15;
			}
			
			if (strPassword.match(/^[\s0-9]+$/)){ 
				bonus.FlatNumber = -35;
			}
		}
			
		function calcComplexity() {
			score = baseScore + (num.Excess*bonus.Excess) + (num.Upper*bonus.Upper) + (num.Numbers*bonus.Numbers) + (num.Symbols*bonus.Symbols) + bonus.Combo + bonus.FlatLower + bonus.FlatNumber;
			
		}	
			
		function outputResult() {

			if ( $("#inputPassword").val()== "" ) {
				complexity.html("Enter a random value").removeClass("weak strong stronger strongest").addClass("default");
			} else if (charPassword.length < minPasswordLength) {
				complexity.html("At least " + minPasswordLength+ " characters please!").removeClass("strong stronger strongest").addClass("weak");
			} else if (score<50) {
				complexity.html("Weak!").removeClass("strong stronger strongest").addClass("weak");
			} else if (score>=50 && score<75) {
				complexity.html("Average!").removeClass("stronger strongest").addClass("strong");
			} else if (score>=75 && score<100) {
				complexity.html("Strong!").removeClass("strongest").addClass("stronger");
			} else if (score>=100) {
				complexity.html("Secure!").addClass("strongest");
			}
			
		}

    };
    /**
     *  End Password strength
     */

   
    /**
     *  Start Like Dislike
     */
     $.fn.depcVote = function(options)
     {
        // Default Options
        var settings = $.extend(
        {
                // These are the defaults.
                id: 0,
                nounce : '',
                like : true
            }, options);

        // Init vote systems
        initLikeDislike(); 

        function initLikeDislike() {

            $(".dpr-discu-container_"+settings.id+" .dpr-discu-box .dpr-discu-box-footer-metadata-like .dpr-cont-discu-"+settings.like+" .dpr-discu-"+settings.like).on('click', function(e){
            	e.preventDefault();
            	var $this = $(this);
				var comment_id = $this.parent().parent().data('id');
				var child = parseInt( $this.children( ".dpr-discu-"+settings.like+"-count" ).text() );
				$this.children( ".dpr-discu-"+settings.like+"-count" ).text( child + 1 );

				$.ajax( {
					type: "POST",
					url: dpr.adminajax,
					data: {
						action   : 'dpr_vote',
						security : settings.nounce,
						comment_id : comment_id,
						like : settings.like
					},
				}).done( function(data) {
					if ( data != 1 ) { $this.children( ".dpr-discu-"+settings.like+"-count" ).text( child - 0 ); }
				}).fail(function(){
					$this.children( ".dpr-discu-"+settings.like+"-count" ).text( child - 0 );
				});

			});	
        }
    }; 
     /**
     *  End Like Dislike
     */
    
    /**
     *  Start Comments Filter
     */
     $.fn.depcFilter = function(options)
     {
        // Default Options
        var settings = $.extend(
        {
                // These are the defaults.
                id: 0,
                nounce : '',
                action : '',
                type : ''
            }, options);

        // Init Search and Filter
        if ( settings.type == 'trending' ) {
        	initTrending(); 
        } else if ( settings.type === 'popular' ) {
        	initPopular(); 
        } else if ( settings.type === 'oldest' ) {
        	initOldest(); 
        } else if ( settings.type === 'newest' ) {
        	initNewest(); 
        }

        function initTrending() {

            $(".dpr-discu-container_"+settings.id+" .dpr-switch-tab-wrap .dpr-switch-tab .dpr-switch-tab-trending .dpr-switch-tab-trending-a").on('click', function(e){
            	e.preventDefault();

            	var $this = $(this);
            	$('.dpr-preloader-wrap').css('display', 'block');

				$.ajax( {
					type: "POST",
					url: dpr.adminajax,
					data: {
						action   : settings.action,
						security : settings.nounce,
						type : settings.type 
					},
				}).done( function(data) {
					$('.dpr-preloader-wrap').css('display', 'none');
					$('.dpr-discu-main-loop-wrap').empty();
					$('.dpr-discu-main-loop-wrap').append(data);
				}).fail(function(){
					// $this.children( ".dpr-discu-"+settings.like+"-count" ).text( child - 0 );
				});

			});	
        }
        function initPopular() {

            $(".dpr-discu-container_"+settings.id+" .dpr-switch-tab-wrap .dpr-switch-tab .dpr-switch-tab-trending .dpr-switch-tab-trending-a").on('click', function(e){
            	e.preventDefault();

            	var $this = $(this);
            	$('.dpr-preloader-wrap').css('display', 'block');

				$.ajax( {
					type: "POST",
					url: dpr.adminajax,
					data: {
						action   : settings.action,
						security : settings.nounce,
						type : settings.type 
					},
				}).done( function(data) {

					$('.dpr-preloader-wrap').css('display', 'none');
					$('.dpr-discu-main-loop-wrap').empty();
					$('.dpr-discu-main-loop-wrap').append(data);
				}).fail(function(){
					$('.dpr-preloader-wrap').css('display', 'none');
				});

			});	
        }
        function initOldest() {

            $(".dpr-discu-container_"+settings.id+" .dpr-switch-tab-wrap .dpr-switch-tab .dpr-switch-tab-oldest .dpr-switch-tab-oldest-a").on('click', function(e){
            	e.preventDefault();
            	console.log( settings.id );
            	var $this = $(this);
            	$('.dpr-preloader-wrap').css('display', 'block');

				$.ajax( {
					type: "POST",
					url: dpr.adminajax,
					data: {
						action   : settings.action,
						security : settings.nounce,
						type : settings.type,
						id : settings.id 
					},
				}).done( function(data) {

					$('.dpr-preloader-wrap').css('display', 'none');
					$('.dpr-container .dpr-switch-tab-wrap .dpr-switch-tab li a').removeClass('dpr-active-tab');
					$this.addClass('dpr-active-tab');
					$('.dpr-discu-main-loop-wrap').empty();
					$('.dpr-discu-main-loop-wrap').append(data);
					$(".dpr-discu-container_"+settings.id+" .dpr-discu-main-loop-wrap .dpr-loadmore-btn").attr('data-type', 'ASC');
				}).fail(function(){
					$('.dpr-preloader-wrap').css('display', 'none');
				});

			});	
        }
        function initNewest() {

            $(".dpr-discu-container_"+settings.id+" .dpr-switch-tab-wrap .dpr-switch-tab .dpr-switch-tab-newest .dpr-switch-tab-newest-a").on('click', function(e){
            	e.preventDefault();

            	var $this = $(this);
            	$('.dpr-preloader-wrap').css('display', 'block');

				$.ajax( {
					type: "POST",
					url: dpr.adminajax,
					data: {
						action   : settings.action,
						security : settings.nounce,
						type : settings.type,
						id : settings.id
					},
				}).done( function(data) {
					$('.dpr-preloader-wrap').css('display', 'none');
					$('.dpr-container .dpr-switch-tab-wrap .dpr-switch-tab li a').removeClass('dpr-active-tab');
					$this.addClass('dpr-active-tab');
					$('.dpr-discu-main-loop-wrap').empty();
					$('.dpr-discu-main-loop-wrap').append(data);
					$(".dpr-discu-container_"+settings.id+" .dpr-discu-main-loop-wrap .dpr-loadmore-btn").attr('data-type', 'DESC');
				}).fail(function(){
					$('.dpr-preloader-wrap').css('display', 'none');
				});

			});	
        }
    }; 
     /**
     *  End Comments Filter
     */
    
    /**
     *  Start Load More
     */
	$.fn.depcLoadMore = function(options) {
		// Default Options
		var settings = $.extend( {
			// These are the defaults.
			id: 0,
			nounce : '',
			action : ''
		}, options);

		// Init load more
		initLoadMore();

		function initLoadMore() {

			$(document).on( 'click', ".dpr-discu-container_"+settings.id+" .dpr-discu-main-loop-wrap .dpr-loadmore-btn", function(e){
				e.preventDefault();

				var $this = $(this) , half_cm = $this.data('comments') , loaded_cm = $this.data('loaded') , type = $this.data('type'); 
				$('.dpr-preloadmore-wrap').css('display', 'block');

				$this.css('display', 'none');

				$.ajax( {
					type: "POST",
					url: dpr.adminajax,
					data: {
						id   : settings.id,
						action   : settings.action,
						security : settings.nounce,
						half_cm : half_cm,
						loaded_cm : loaded_cm,
						type : type
					},
				}).done( function(data) {
					$('.dpr-preloadmore-wrap').css('display', 'none');
					$(data.data).insertAfter('.dpr-container .dpr-discu-main-loop-wrap .dpr-insertafter:last');
					$this.remove();
					$('.dpr-discu-main-loop-wrap .dpr-loadmore-btn').attr( 'data-comments', data.half_cm );
					$('.dpr-discu-main-loop-wrap .dpr-loadmore-btn').attr( 'data-loaded', data.loaded_cm );

					if ( parseInt( $('.dpr-discu-main-loop-wrap .dpr-loadmore-btn').data('comments') ) == 0 ) { 
						$('.dpr-discu-main-loop-wrap .dpr-loadmore-btn').remove();
					}

				}).fail(function(){
					$this.css('display', 'block');
					$('.dpr-preloadmore-wrap').css('display', 'none');
				});

			});	
		}
	}; 
     /**
     *  End Load More
     */

	/**
     *  Start tiny dom actions
     */
	$(document).ready(function($) {
		// copy link
		$(document).on('click', '.dpr-discu-wrap .dpr-discu-link', function(event) {
			event.preventDefault();
			new Clipboard('.dpr-discu-link');
		});

		$(document).on('click', '.dpr-discu-box .dpr-discu-collapse', function(event) {
			event.preventDefault();
			var comment_id = $(this).data('id');
			$( '.dpr-discu-wrap_' + comment_id+ ' .dpr-c-contents' ).toggle( 'fast' );
		});

		// cancle button on comments
		$(document).on('click', '.dpr-discu-wrap .dpr-discu-text .dpr_cancle_comment', function(event) {
			event.preventDefault();
			var $this = $(this) , comment_id = $this.data('id') , parent_id = $this.data('parent') , selector = '';
			$( ".dpr-discu-wrap_"+comment_id+" .dpr-discu-box-header .dpr-discu-edit" ).first().trigger('click'); 
		});

		// cancle button on reply comments
		$(document).on('click', '.dpr-discu-wrap .dpr-discu-replies-wrap .dpr_cancle_reply_comment', function(event) {
			event.preventDefault();
			var $this = $(this) , comment_id = $this.data('id') , parent_id = $this.data('parent') , selector = '';
			if ( comment_id === parent_id ) $( ".dpr-discu-wrap_"+comment_id+" .dpr-discu-box-footer .dpr-discu-reply-btn-wrap .dpr-discu-reply-btn" ).first().trigger('click'); else  $( ".dpr-discu-wrap_"+comment_id+" .dpr-discu-box-footer .dpr-discu-reply-btn-wrap .dpr-discu-reply-btn" ).trigger('click');
		});		


	});
	/**
     *  End tiny dom actions
	*/

    /**
     *  Start tiny MCE Config
     */
     function dpr_tinymce( selector , height , status , size ) {

     	tinymce.init({
     		selector: selector,
     		height: height,
            branding: false,
            statusbar: status,
     		theme: 'modern',
     		plugins: [
     		"wpemoji textcolor colorpicker"
     		],
     		toolbar1: "bold | italic | underline | strikethrough | outdent | indent | blockquote | undo | redo  |  forecolor | backcolor  | wpemoji",

     		menubar: false,
     		toolbar_items_size: size,

     		style_formats: [{
     			title: 'Bold text',
     			inline: 'b'
     		}, {
     			title: 'Red text',
     			inline: 'span',
     			styles: {
     				color: '#ff0000'
     			}
     		}, {
     			title: 'Red header',
     			block: 'h1',
     			styles: {
     				color: '#ff0000'
     			}
     		}, {
     			title: 'Example 1',
     			inline: 'span',
     			classes: 'example1'
     		}, {
     			title: 'Example 2',
     			inline: 'span',
     			classes: 'example2'
     		}, {
     			title: 'Table styles'
     		}, {
     			title: 'Table row 1',
     			selector: 'tr',
     			classes: 'tablerow1'
     		}],

     		templates: [{
     			title: 'Test template 1',
     			content: 'Test 1'
     		}, {
     			title: 'Test template 2',
     			content: 'Test 2'
     		}]
     	});
     }
    /**
     *  End tiny MCE Config
     */


 }(jQuery));