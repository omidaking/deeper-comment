<?php

/**
 * @link       http://webnus.biz
 * @since      1.0.0
 *
 * @package    Depper Comment
 */
echo $scripts;
?>
<?php if( $settings['quick_login'] == 'on' ): ?>
	<a href="#" class="dpr-join-form-login-a"><i class="sl-login"></i> <?php esc_attr_e( 'Login', 'depc' ); ?></a>
<?php endif; ?>

<?php if( $settings['quick_register'] == 'on' ): ?>
	<a href="#" class="dpr-join-form-register-a"><i class="sl-plus"></i> <?php esc_attr_e( 'Register', 'depc' ); ?></a>
<?php endif; ?>
