<?php

/**
 * @link       http://webnus.biz
 * @since      1.0.0
 *
 * @package    Depper Comment
 */

?>
<!-- Start comment Template
	================================================== -->
	<div class='dpr-preloader-wrap'>
		<div class='dpr-preloader'></div>
	</div>
	<?php

	$comment = Depc_Controller_Public_Comment::get_instance();
	$comment->load();
	?>

<!-- End Start comment Template
	================================================== -->
