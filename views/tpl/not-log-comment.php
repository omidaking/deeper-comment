<?php

/**
 * @link       http://webnus.biz
 * @since      1.0.0
 *
 * @package    Depper Comment
 */
?>


<!-- Primary Page Layout
	================================================== -->
	<div class="dpr-wrap">
		<div class="dpr-container dpr-discu-container dpr-discu-container_<?php echo $post; ?>">
		<?php 
		$edit->load();
		$filter->load();
		?>
			<div class="dpr-join-form-wrap"><!-- deeper switch-tab wrapper /start -->

				<div class="dpr-join-form">
					<div class="dpr-join-form-area">
						<i class="sl-user"></i>
						<textarea placeholder="Join the discussion..."></textarea>
					</div>
					<div class="dpr-join-form-inner">

						<div class="dpr-join-form-login-register">
							<?php $login = Depc_Controller_Module_Login::get_instance(); $login->load_login(); ?> 
						</div>

						<div class="dpr-join-form-social-login">
							<?php $google = Depc_Controller_Module_Google::get_instance(); $google->load(); ?>							
						</div>
					</div>
				</div>
			</div><!-- deeper switch-tab wrapper /end -->
			<div class="dpr-discu-main-loop-wrap">
				<?php 
				$loop = Depc_Controller_Public_Comment_Loop::get_instance();
				$loop->load();
				?>
			</div>
		</div><!-- end dpr-container -->
	</div><!-- end dpr-wrap -->


<!-- End Document
	================================================== -->
