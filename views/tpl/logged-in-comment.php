<?php
/**
 * Created by PhpStorm.
 * User: Webnus01
 * Date: 6/25/2017
 * Time: 11:21 AM
 */
?>
<!-- Primary Page Layout
================================================== -->
<div class="dpr-wrap">
    <div class="dpr-container dpr-discu-container_<?php echo $post; ?>">
		<?php 
		$edit->load();
		$filter->load();
		?>
	    <div class="dpr-join-form-wrap"><!-- deeper switch-tab wrapper /start -->

		    <div class="dpr-join-form">
			    <div class="dpr-join-form-area"><div class="dpr-discu-user">
					    <div class="dpr-discu-user-img"><?php echo $avatar; ?></div>

				    </div> <textarea placeholder="Join the discussion..."></textarea></div>
			    <div class="dpr-join-form-inner">
				    <div class="dpr-join-form-login-register">
					    <a href="<?php echo $user_profile_link; ?>" class="dpr-discu-user-a"><i class="sl-user"></i> <?php echo $username; ?></a>
				    </div>
			    </div>
		    </div>
	    </div><!-- deeper switch-tab wrapper /end -->
	    <div class="dpr-discu-main-loop-wrap">
			<?php 
			$loop = Depc_Controller_Public_Comment_Loop::get_instance();
			$loop->load();
			 ?>
		</div>
	</div><!-- end dpr-container -->
</div><!-- end dpr-wrap -->



<!-- End Document
================================================== -->
